<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKkDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kk_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('id_kk')->constrained('kk')->onDelete('cascade');
            $table->foreignId('id_pengamal')->constrained('pengamal')->onDelete('cascade');
            $table->string('user_as', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kk_detail');
    }
}
