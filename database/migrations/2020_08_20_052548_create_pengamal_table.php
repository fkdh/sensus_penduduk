<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengamalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengamal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('ktp_path')->nullable();
            $table->char('nik', 50)->unique();
            $table->date('birth_date');
            $table->unsignedBigInteger('kel_id');
            $table->unsignedBigInteger('kec_id');
            $table->unsignedBigInteger('kab_id');
            $table->unsignedBigInteger('prov_id');
            // $table->string('address', 100);
            $table->unsignedTinyInteger('job_id');
            $table->unsignedTinyInteger('education_id');
            $table->unsignedTinyInteger('salary_id');
            $table->string('blood', 50);
            $table->string('gender', 50);
            $table->string('married', 50);
            $table->boolean('has_die')->default(0);
            $table->date('die_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengamal');
    }
}
