<?php

use Illuminate\Database\Seeder;
use App\Education;

class EducationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$data = [
			['education'=>'Tidak / Belum Sekolah'],
			['education'=>'PAUD'],
			['education'=>'TK / Sederajat'],
			['education'=>'Putus SD'],
			['education'=>'SD / Sederajat'],
			['education'=>'SMP / Sederajat'],
			['education'=>'SMA / Sederajat'],
			['education'=>'Paket A'],
			['education'=>'Paket B'],
			['education'=>'Paket C'],
			['education'=>'D1'],
			['education'=>'D2'],
			['education'=>'D3'],
			['education'=>'D4'],
			['education'=>'S1'],
			['education'=>'Profesi'],
			['education'=>'Sp-1'],
			['education'=>'S2'],
			['education'=>'S2 Terapan'],
			['education'=>'Sp-2'],
			['education'=>'S3'],
			['education'=>'S3 Terapan'],
			['education'=>'Non Formal'],
			['education'=>'Lainnya'],
		];

		Education::insert($data);
		
		/*foreach ($data as $row) {
        	Education::insert($row);
        }*/


    }
}
