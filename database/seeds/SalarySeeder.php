<?php

use Illuminate\Database\Seeder;
use App\Salary;

class SalarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$data = [
			['name'=>'Kurang dari Rp.500,000'],
			['name'=>'Rp.500,000 - Rp. 999,999'],
			['name'=>'Rp.1,000,000 - Rp. 1,999,999'],
			['name'=>'Rp.2,000,000 - Rp. 4,999,999'],
			['name'=>'Rp.5,000,000 - Rp. 20,000,000'],
			['name'=>'Lebih dari Rp.20,000,000'],
		];

		Salary::insert($data);
		
		/*foreach ($data as $row) {
        	Education::insert($row);
        }*/


    }
}
