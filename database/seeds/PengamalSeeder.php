<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

use App\Pengamal;
use App\Provinsi;
use App\Kabupaten;
use App\Kecamatan;
use App\Kelurahan;
use App\Job;
use App\Education;
use App\Salary;

class PengamalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$faker = Faker::create();
        $data= [];

        $job = config('job');
        $education = config('global.education');
        $blood = config('global.blood');
        $married = config('global.married');
        $gender = config('global.gender');

        $x = Pengamal::count() + 1;

        for ($j=0; $j < 10; $j++) { 
        	$data= [];
	        for ($i=0; $i < 1000; $i++) { 
		        $provinsi = Provinsi::all()->random(1)->first();
		        $kabupaten = Kabupaten::where('id_prov', $provinsi->id)->inRandomOrder()->first();
		        $kecamatan = Kecamatan::where('id_kab', $kabupaten->id)->inRandomOrder()->first();
		        $kelurahan = Kelurahan::where('id_kec', $kecamatan->id)->inRandomOrder()->first();
		        $job = Job::all()->random(1)->first();
		        $education = Education::all()->random(1)->first();
		        $salary = Salary::all()->random(1)->first();

	        	$data[] = [
		            'name' => $faker->name,
		            'nik' => $x,
		            'gender' => array_rand($gender,1),
		            'birth_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
		            'kel_id' => $kelurahan['id'],
		            'kec_id' => $kecamatan['id'],
		            'kab_id' => $kabupaten['id'],
		            'prov_id' => $provinsi->id,
		            // 'address' => $faker->address,
		            'education_id' => $education->id,
		            'blood' => array_rand($blood,1),
		            'married' => array_rand($married,1),
		            'job_id' => $job->id,
		            'salary_id' => $salary->id,
		            'has_die' => 0,
		            'die_date' => null,
		            'created_at' => now()->toDateTimeString(),
		            'updated_at' => now()->toDateTimeString(),
		        ];

		        $x += 1;
	        }

	        DB::disableQueryLog();
	        DB::table('pengamal')->insert($data);


	        // Pengamal::insert($data);
	        
	        // $chunks = array_chunk($data, 10);

	        // foreach ($chunks as $row) {
	        // 	Pengamal::insert($row);
	        // }
	        
        }


        // $chunks = array_chunk($data, 10);
        // Pengamal::insert($data);


        // foreach ($chunks as $row) {
        // 	Pengamal::insert($row);
        // }

        
    }
}
