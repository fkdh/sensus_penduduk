<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

use App\Pengamal;
use App\Kk;
use App\KkDetail;

class KkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$faker = Faker::create();
        $data= [];

        $x = KkDetail::count() + 1;
        $count_kk = Kk::count() + 1;

        for ($j=0; $j < 3; $j++) { 
        	$data_kk= [];
        	$data_detail_kk = [];
	        for ($i=0; $i < 1000; $i++) { 
	        	$data_kk[] = [
	        		'id' => $count_kk,
		            'kk_path' => null,
		            'no_kk' => $count_kk,
		            'id_pengamal' => $x,
		            'created_at' => now()->toDateTimeString(),
		            'updated_at' => now()->toDateTimeString(),
		        ];

		        for ($k=0; $k < 3; $k++) { 
		        	$user_as= '';
		        	if($k == 0) {$user_as = 'Ayah';} else{$user_as = 'Anak';}
		        	$data_detail_kk[] = [
		        		'id_kk' => $count_kk, 
		        		'id_pengamal' => $x,
		        		'user_as' => $user_as,
		        		'created_at' => now()->toDateTimeString(),
			            'updated_at' => now()->toDateTimeString(),
		        	];
		        	$x++;
		        }

		        $count_kk++;
	        }

	        DB::disableQueryLog();
	        DB::table('kk')->insert($data_kk);
	        DB::table('kk_detail')->insert($data_detail_kk);


	        // Pengamal::insert($data);
	        
	        // $chunks = array_chunk($data, 10);

	        // foreach ($chunks as $row) {
	        // 	Pengamal::insert($row);
	        // }
	        
        }


        // $chunks = array_chunk($data, 10);
        // Pengamal::insert($data);


        // foreach ($chunks as $row) {
        // 	Pengamal::insert($row);
        // }

        
    }
}
