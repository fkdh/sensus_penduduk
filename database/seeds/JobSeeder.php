<?php

use Illuminate\Database\Seeder;
use App\Job;

class JobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*$data = [
		    ['job'=>'BELUM/TIDAK BEKERJA'],
		    ['job'=>'MENGURUS RUMAH TANGGA'],
		    ['job'=>'PELAJAR/MAHASISWA'],
		    ['job'=>'PENSIUNAN'],
		    ['job'=>'PEGAWAI NEGERI SIPIL'],
		    ['job'=>'TENTARA NASIONAL INDONESIA'],
		    ['job'=>'KEPOLISIAN RI'],
		    ['job'=>'PERDAGANGAN'],
		    ['job'=>'PETANI/PEKEBUN'],
		    ['job'=>'PETERNAK'],
		    ['job'=>'NELAYAN/PERIKANAN'],
		    ['job'=>'INDUSTRI'],
		    ['job'=>'KONSTRUKSI'],
		    ['job'=>'TRANSPORTASI'],
		    ['job'=>'KARYAWAN SWASTA'],
		    ['job'=>'KARYAWAN BUMN'],
		    ['job'=>'KARYAWAN BUMD'],
		    ['job'=>'KARYAWAN HONORER'],
		    ['job'=>'BURUH HARIAN LEPAS'],
		    ['job'=>'BURUH TANI/PERKEBUNAN'],
		    ['job'=>'BURUH NELAYAN/PERIKANAN'],
		    ['job'=>'BURUH PETERNAKAN'],
		    ['job'=>'PEMBANTU RUMAH TANGGA'],
		    ['job'=>'TUKANG CUKUR'],
		    ['job'=>'TUKANG LISTRIK'],
		    ['job'=>'TUKANG BATU'],
		    ['job'=>'TUKANG KAYU'],
		    ['job'=>'TUKANG SOL SEPATU'],
		    ['job'=>'TUKANG LAS/PANDAI'],
		    ['job'=>'TUKANG JAHIT'],
		    ['job'=>'TUKANG GIGI'],
		    ['job'=>'PENATA RIAS'],
		    ['job'=>'PENATA BUSANA'],
		    ['job'=>'PENATA RAMBUT'],
		    ['job'=>'MEKANIK'],
		    ['job'=>'SENIMAN'],
		    ['job'=>'TABIB'],
		    ['job'=>'PARAJI'],
		    ['job'=>'PERANCANG BUSANA'],
		    ['job'=>'PENTERJEMAH'],
		    ['job'=>'IMAM MESJID'],
		    ['job'=>'PENDETA'],
		    ['job'=>'PASTOR'],
		    ['job'=>'WARTAWAN'],
		    ['job'=>'USTADZ/MUBALIGH'],
		    ['job'=>'JURU MASAK'],
		    ['job'=>'PROMOTOR ACARA'],
		    ['job'=>'ANGGOTA DPR-RI'],
		    ['job'=>'ANGGOTA DPD'],
		    ['job'=>'ANGGOTA BPK'],
		    ['job'=>'PRESIDEN'],
		    ['job'=>'WAKIL PRESIDEN'],
		    ['job'=>'ANGGOTA MAHKAMAH KONSTITUSI'],
		    ['job'=>'ANGGOTA KABINET/KEMENTERIAN'],
		    ['job'=>'DUTA BESA'],
		    ['job'=>'GUBERNUR'],
		    ['job'=>'WAKIL GUBERNUR'],
		    ['job'=>'BUPATI'],
		    ['job'=>'WAKIL BUPATI'],
		    ['job'=>'WALIKOTA'],
		    ['job'=>'WAKIL WALIKOTA'],
		    ['job'=>'ANGGOTA DPRD PROVINSI'],
		    ['job'=>'ANGGOTA DPRD KABUPATEN'],
		    ['job'=>'DOSEN'],
		    ['job'=>'GURU'],
		    ['job'=>'PILOT'],
		    ['job'=>'PENGACARA'],
		    ['job'=>'NOTARIS'],
		    ['job'=>'ARSITEK'],
		    ['job'=>'AKUNTAN'],
		    ['job'=>'KONSULTAN'],
		    ['job'=>'DOKTER'],
		    ['job'=>'BIDAN'],
		    ['job'=>'PERAWAT'],
		    ['job'=>'APOTEKER'],
		    ['job'=>'PSIKIATER/PSIKOLOG'],
		    ['job'=>'PENYIAR TELEVISI'],
		    ['job'=>'PENYIAR RADIO'],
		    ['job'=>'PELAUT'],
		    ['job'=>'PENELITI'],
		    ['job'=>'SOPIR'],
		    ['job'=>'PIALANG'],
		    ['job'=>'PARANORMAL'],
		    ['job'=>'PEDAGANG'],
		    ['job'=>'PERANGKAT DESA'],
		    ['job'=>'KEPALA DESA'],
		    ['job'=>'BIARAWATI'],
		    ['job'=>'WIRASWASTA'],
		    ['job'=>'LAINNYA'],
		];*/

		$data = [
			['job' => 'Tidak / Belum Bekerja'],
			['job' => 'Nelayan'],
			['job' => 'Petani'],
			['job' => 'Peternak'],
			['job' => 'PNS/TNI/Porli'],
			['job' => 'Karyawan Swasta'],
			['job' => 'Pedagang Kecil'],
			['job' => 'Pedagang Besar'],
			['job' => 'Wirawsasta'],
			['job' => 'Wirausaha'],
			['job' => 'Buruh'],
			['job' => 'Pensiunan'],
			['job' => 'Peneliti'],
			['job' => 'Tim Ahli / Konsultan'],
			['job' => 'Magang'],
			['job' => 'Tenaga Pengajar / Instruktur / Fasilitator'],
			['job' => 'Pimpinan / Manajerial'],
			['job' => 'Lainnya'],
		];

		Job::insert($data);

		// foreach ($data as $row) {
  //       	Job::insert($row);
  //       }
    }
}
