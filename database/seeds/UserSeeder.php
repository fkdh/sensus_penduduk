<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Role;
use App\AgeRange;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = [
            ['role' => 'Super Admin'],
            ['role' => 'Admin'],
            ['role' => 'Admin Provinsi'],
            ['role' => 'Admin Kabupaten'],
            ['role' => 'Admin Kecamatan'],
            ['role' => 'Admin Desa'],
        ];

        $age_ranges = [
            ['name' => 'Kanak-Kanak', 'min_age' => 0, 'max_age' => 14],
            ['name' => 'Remaja', 'min_age' => 15, 'max_age' => 30],
            ['name' => 'Dewasa', 'min_age' => 31, 'max_age' => 100],
        ];

        $data = [
            'name' => 'Tes Admin',
            'phone' => '08717',
            'email' => 'admin@gmail.com',
            'password' => Hash::make(1),
            'role' => 1,
            'is_active' => 1,
        ];

        // AgeRange::insert($age_ranges);
        // Role::insert($role);
        User::create($data);
    }
}
