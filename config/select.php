<?php

return [
	'blood' => [
		'' => 'pilih Gol. Darah',
		'A' => 'A',
		'B' => 'B',
		'AB' => 'AB',
		'O' => 'O',
	],
	'married' => [
		'' => 'pilih Status',
		'BELUM KAWIN' => 'BELUM KAWIN',
		'KAWIN' => 'KAWIN',
	],
	'gender' => [
		'' => 'pilih Jenis Kelamin',
		'Laki-Laki' => 'Laki-Laki',
		'Perempuan' => 'Perempuan',
	],
	'blood' => [
		'' => 'pilih Gol. Darah',
		'A' => 'A',
		'B' => 'B',
		'AB' => 'AB',
		'O' => 'O',
	],
	'is_active' => [
		'1' => 'Active',
		'0' => 'Not Active',
	],
	'has_die' => [
		'' => 'pilih status hidup',
		'1' => 'Sudah Meninggal',
		'0' => 'Masih Hidup',
	],
	'status_keluarga' => [
		'' => 'pilih Status Keluarga',
		'Ibu' => 'Ibu',
		'Anak' => 'Anak',
	],
	'jamaah' => [
		'' => 'pilih Jamaah',
		'Kanak-Kanak' => 'Kanak-Kanak',
		'Remaja' => 'Remaja',
		'Ibu-Ibu' => 'Ibu-Ibu',
		'Bapak-Bapak' => 'Bapak-Bapak',
	],
];