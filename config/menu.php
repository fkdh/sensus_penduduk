<?php

return [
	
	/*
    |--------------------------------------------------------------------------
    | Application Menu
    |--------------------------------------------------------------------------
    |
    | Main menu For Apps
    |     |
    */

    'main_menu' => [
		"dashboard" => [
            "class" => "nav-icon far fas fa-tachometer-alt",
            "desc" => "Dashboard",
            "main_url" => "/",
            "sub_menu" => "",
        ],
        "pengamal" => [
            "class" => "nav-icon far fa fa-user",
            "desc" => "Pengamal",
            "main_url" => "/pengamal",
            "sub_menu" => "",
        ],
        "kk" => [
            "class" => "nav-icon far fa fa-users",
            "desc" => "KK",
            "main_url" => "/kk",
            "sub_menu" => "",
        ],
        "count_pengamal" => [
            "class" => "nav-icon far fa fa-users",
            "desc" => "Jumlah Pengamal",
            "main_url" => "/count-pengamal",
            "sub_menu" => "",
        ],
        "master_data" => [
            "class" => "nav-icon far fa fa-cog",
            "desc" => "Master Data",
            "main_url" => "#",
            "sub_menu" => [
                "admin" => [
                    "class" => "nav-icon far fas fa-user-cog",
                    "desc" => "Admin",
                    "main_url" => "/master-data/admin",
                    "sub_menu" => "",
                ],
                "role" => [
                    "class" => "nav-icon far fas fa-cog",
                    "desc" => "Role",
                    "main_url" => "/master-data/role",
                    "sub_menu" => "",
                ],
                "age_range" => [
                    "class" => "nav-icon far fas fa-cog",
                    "desc" => "Age Range",
                    "main_url" => "/master-data/age-range",
                    "sub_menu" => "",
                ],
            ],
        ],
    ],
	
];