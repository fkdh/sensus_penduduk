<?php

return [
	'education' => [
		'1'=>'TAMAT SD / SEDERAJAT',
		'2'=>'SLTP/SEDERAJAT',
		'3'=>'TIDAK / BELUM SEKOLAH',
		'4'=>'SLTA / SEDERAJAT',
		'5'=>'BELUM TAMAT SD/SEDERAJAT',
		'6'=>'DIPLOMA IV/ STRATA I',
		'7'=>'AKADEMI/ DIPLOMA III/S. MUDA',
		'8'=>'DIPLOMA I / II',
		'9'=>'STRATA III',
		'10'=>'STRATA II',
	],
	'blood' => [
		'A' => 'A',
		'B' => 'B',
		'AB' => 'AB',
		'O' => 'O',
	],
	'married' => [
		'BELUM KAWIN' => 'BELUM KAWIN',
		'KAWIN' => 'KAWIN',
	],
	'gender' => [
		'Laki-Laki' => 'Laki-Laki',
		'Perempuan' => 'Perempuan',
	],
	'role' => [
		'1' => 'Super Admin',
		'2' => 'Admin',
		'3' => 'Admin Provinsi',
		'4' => 'Admin Kabupaten',
		'5' => 'Admin Kecamatan',
		'6' => 'Admin Kelurahan',
	],
];