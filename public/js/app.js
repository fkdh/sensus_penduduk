$(document).ready(function() {
    $('body').on('click', '.modal-show', function (event) {
        event.preventDefault();
        var me = $(this),
            url = me.attr('href'),
            title = me.attr('title');

        $('#modal-title').text(title);
        $('#modal-btn-save').removeClass('hide')
        .text(me.hasClass('edit') ? 'Update' : 'Create');

        $.ajax({
            url: url,
            dataType: 'html',
            success: function (response) {
                $('#modal-body').html(response);
            }
        });

        $('#modal').modal('show');
    });

    $('#modal-btn-save').click(function (event) {
        setTimeout(function(){ 
            event.preventDefault();

            var form = $('#modal-body form'),
                url = form.attr('action'),
                method = $('input[name=_method]').val() == undefined ? 'POST' : 'PUT';

            form.find('.invalid-feedback').remove();
            form.find('.is-invalid').removeClass('is-invalid');

            $.ajax({
                url : url,
                method: method,
                data : form.serialize(),
                success: function (response) {
                    // return
                    form.trigger('reset');
                    $('#modal').modal('hide');
                    $('#datatable').DataTable().ajax.reload();

                    swal({
                        type : 'success',
                        title : 'Success!',
                        text : 'Data has been saved!'
                    });
                },
                error : function (xhr) {
                    var res = xhr.responseJSON;
                    if ($.isEmptyObject(res) == false) {
                        $.each(res.errors, function (key, value) {
                            $('#' + key)
                                .addClass('is-invalid')
                                .closest('.form-group')
                                .append('<span class="error invalid-feedback">' + value + '</span>');
                        });
                    }
                    swal({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!'
                    });
                }
            })
        }, 100);
    });

    $('body').on('click', '.btn-delete', function (event) {
        event.preventDefault();

        var me = $(this),
            url = me.attr('href'),
            title = me.attr('title'),
            csrf_token = $('meta[name="csrf-token"]').attr('content');

        swal({
            title: 'Are you sure want to delete ' + title + ' ?',
            text: 'You won\'t be able to revert this!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        '_method': 'DELETE',
                        '_token': csrf_token
                    },
                    success: function (response) {
                        $('#datatable').DataTable().ajax.reload();
                        swal({
                            type: 'success',
                            title: 'Success!',
                            text: 'Data has been deleted!'
                        });
                    },
                    error: function (xhr) {
                        swal({
                            type: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!'
                        });
                    }
                });
            }
        });
    });

    $(document).on('click', '#btn-logout', function (event) {
        event.preventDefault();
        var me = $(this),
            url = me.attr('href'),
            csrf_token = $('meta[name="csrf-token"]').attr('content');

        swal({
            title: 'Are you sure want to Logout?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Logout!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        '_token': csrf_token
                    },
                    success: function (response) {
                        location.reload()
                    },
                    error: function (xhr) {
                        swal({
                            type: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!'
                        });
                    }
                });
            }
        });
    });

})

