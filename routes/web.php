<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function() {
	/*Dashboard*/
	Route::get('/', 'Admin\DhasboardController@index');
	/*Pengamal*/
	Route::resource('/pengamal', 'Admin\PengamalController');
	Route::get('/set-die/{id}/pengamal', 'Admin\PengamalController@setDie')->name('pengamal.set-die');
	Route::get('/set-alive/{id}/pengamal', 'Admin\PengamalController@setAlive')->name('pengamal.set-alive');
	Route::post('/store-die/pengamal/{id}', 'Admin\PengamalController@storeDie')->name('pengamal.store-die');
	Route::post('/store-alive/pengamal/{id}', 'Admin\PengamalController@storeAlive')->name('pengamal.store-alive');

	Route::get('/table/pengamal', 'Admin\PengamalController@dataTable')->name('table.user');
	Route::post('/get-region', 'Admin\PengamalController@getRegion')->name('get.region');
	/*KK*/
	Route::resource('/kk', 'Admin\KkController');
	Route::get('/get-pengaml/kk', 'Admin\KkController@getPengamalList')->name('get.pengamal.list');
	Route::get('/table/kk', 'Admin\KkController@dataTable')->name('table.kk');
	/*Kk Detail*/
	Route::resource('/kk-detail', 'Admin\KkDetailController',['except' => ['create']]);
	Route::get('/kk-detail/create/{kk_detail}', 'Admin\KkDetailController@create')->name('kk-detail.create');
	Route::get('/table/kk-detail', 'Admin\KkDetailController@dataTable')->name('table.kk.detail');
	/*Count Pengamal*/
	Route::resource('/count-pengamal', 'Admin\CountPengamalController', ['except' => ['show']]);
	Route::get('/table/count-pengamal', 'Admin\CountPengamalController@dataTable')->name('table.count.pengamal');

	Route::group(['middleware' => ['super_admin']], function() {
		/*masterdata*/
		Route::group(['prefix' => 'master-data'], function() {
			/*admin*/
			Route::resource('/admin', 'Admin\MasterData\UserAdminController', ['except' => ['show']]);
			Route::get('/table/admin', 'Admin\MasterData\UserAdminController@dataTable')->name('table.admin');
			/*role*/
			Route::resource('/role', 'Admin\MasterData\RoleController', ['except' => ['show']]);
			Route::get('/table/role', 'Admin\MasterData\RoleController@dataTable')->name('table.role');
			/*Age Ranges*/
			Route::resource('/age-range', 'Admin\MasterData\AgeRangeController', ['except' => ['show']]);
			Route::get('/table/age-range', 'Admin\MasterData\AgeRangeController@dataTable')->name('table.age.range');
		});
	});

	/*Logout*/
	Route::post('/logout', 'Admin\AuthController@logout')->name('logout');
});

/*auht*/
Route::get('/login', 'Admin\AuthController@login')->name('login');
Route::post('/process-login', 'Admin\AuthController@processLogin')->name('process.login');



