<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengamal extends Model
{
    protected $guarded = [];
    protected $table = 'pengamal';

    public function kelurahan() {
    	return $this->belongsTo('App\Kelurahan', 'kel_id', 'id');
    }

    public function kecamatan() {
    	return $this->belongsTo('App\Kecamatan', 'kec_id', 'id');
    }

    public function kabupaten() {
    	return $this->belongsTo('App\Kabupaten', 'kab_id', 'id');
    }

    public function provinsi() {
    	return $this->belongsTo('App\Provinsi', 'prov_id', 'id');
    }

    public function job() {
        return $this->belongsTo('App\Job', 'job_id', 'id');
    }

    public function education() {
        return $this->belongsTo('App\Education', 'education_id', 'id');
    }

    public function salary() {
        return $this->belongsTo('App\Salary', 'salary_id', 'id');
    }
}
