<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $guarded = [];
    protected $table = 'job';

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
