<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgeRange extends Model
{
    protected $guarded = [];
    protected $table = 'age_ranges';
}
