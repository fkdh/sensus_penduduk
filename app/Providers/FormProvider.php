<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Form;

class FormProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Form::component('labelRaw' , 'components.form.labelraw' , ['name' , 'label' => null , 'value' => null , 'attributes'=>[]]);
        Form::component('textRaw' , 'components.form.textraw' , ['name' , 'label' => null , 'value' => null , 'attributes'=>[]]);
        Form::component('textCol' , 'components.form.textcol' , ['name' , 'label' => null , 'value' => null , 'attributes'=>[]]);
        Form::component('textAreaRaw' , 'components.form.textarearaw' , ['name' , 'label'=>null , 'value' => null , 'attributes'=>[]]);
        Form::component('textAreaCol' , 'components.form.textareacol' , ['name' , 'label'=>null , 'value' => null , 'attributes'=>[]]);
        Form::component('passRaw' , 'components.form.passraw' , ['name' , 'label'=>null , 'value' => null , 'attributes'=>[]]);
        Form::component('passCol' , 'components.form.passcol' , ['name' , 'label'=>null , 'value' => null , 'attributes'=>[]]);
        Form::component('dropdownCol' , 'components.form.dropcol' , ['name' , 'label'=> null , 'list_dropdown' => [],'value' => null , 'attributes'=>[]]);
        Form::component('dropdownRaw' , 'components.form.dropraw' , ['name' , 'label'=> null , 'list_dropdown' => [],'value' => null , 'attributes'=>[]]);
        Form::component('rawDropdown' , 'components.form.dropraw_custom' , ['name' , 'label'=> null , 'list_dropdown' => [],'value' => null , 'attributes'=>[]]);
        Form::component('fileCol' , 'components.form.filecol' , ['name' , 'label'=> null ,'attributes'=>[]]);
        Form::component('fileRaw' , 'components.form.fileraw' , ['name' , 'label'=> null ,'attributes'=>[]]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
