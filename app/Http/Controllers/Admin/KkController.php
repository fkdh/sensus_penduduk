<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use DataTables;
use Carbon\Carbon;
use Auth;
use GlobalHelper;

use App\Kk;
use App\Pengamal;
use App\Provinsi;
use App\Kabupaten;
use App\Kecamatan;
use App\Kelurahan;

class KkController extends Controller
{
    public function index()
    {
        $filter_location = $this->_getFilterLocation('layouts.component._filter_location');
        return view('kk.list', compact('filter_location'));
    }

    public function create()
    {
        $model = New Kk;
        return view('kk.form', compact('model'));
    }

    private function _validator($data) {
        return  Validator::make($data, [
            'no_kk' => 'required|numeric|unique:kk,no_kk',
            'id_pengamal' => 'required|unique:kk,id_pengamal',
            'kk_path_send' => 'required',
        ], [
            'required' => ':attribute tidak boleh kosong',
            'id_pengamal.unique' => ':attribute sudah terdaftar sebagai Kepala Keluarga',
            'no_kk.unique' => ':attribute sudah terdaftar',
            'numeric' => ':attribute harus berupa angka',
        ], [
            'id_pengamal' => 'Nama / NIK',
            'no_kk' => 'No. KK',
        ])->validate();
    }

    public function store(Request $request)
    {
        $validated = $this->_validator($request->all());
        unset($validated['kk_path_send']);

        if($request->upload_kk) {
            $img_path = GlobalHelper::uploadImage('kk',$request->upload_kk);
            $validated['kk_path'] = $img_path;
        }

        $model = Kk::create($validated);
        $model->kk_detail()->create([
            'id_kk' => $model->id,
            'id_pengamal' => $model->id_pengamal,
            'user_as' => 'Ayah',
        ]);
        return $model;
    }

    public function show($id)
    {
        $model = Kk::findOrFail($id);
        return view('kk_detail.list', compact('model'));
    }

    public function edit($id)
    {
        $model = Kk::findOrFail($id);
        return view('kk.form', compact('model'));
    }

    private function _validator_update($data, $id) {
        return  Validator::make($data, [
            'no_kk' => 'required|numeric|unique:kk,no_kk,'.$id,
        ], [
            'required' => ':attribute tidak boleh kosong',
            'unique' => ':attribute sudah terdaftar',
            'numeric' => ':attribute harus berupa angka',
        ], [
            'no_kk' => 'No. KK',
        ])->validate();
    }

    public function update(Request $request, $id)
    {
        $validated = $this->_validator_update($request->all(), $id);

        $model = Kk::findOrFail($id);

        if($request->upload_kk) {
            $path_delete = $model->kk_path;
            $img_path = GlobalHelper::uploadImage('kk',$request->upload_kk, $path_delete);
            $validated['kk_path'] = $img_path;
        }
        $model->update($validated);
        return $model;
    }

    public function destroy($id)
    {
        $model = Kk::findOrFail($id);
        $model->delete();
    }

    public function dataTable(Request $request) {
        // return response()->json($request);
        // $model = Kk::with(['pengamal'])->get();
        $model = Kk::join('pengamal', 'pengamal.id', '=', 'kk.id_pengamal')
                ->select('kk.no_kk','kk.id as id_kk', 'pengamal.*');

        if(!empty($request->id_prov)) {
            $model->where('pengamal.prov_id', $request->id_prov);
        } 

        if(!empty($request->id_kab)) {
            $model->where('pengamal.kab_id', $request->id_kab);
        } 

        if(!empty($request->id_kec)) {
            $model->where('pengamal.kec_id', $request->id_kec);
        } 

        if(!empty($request->id_kel)) {
            $model->where('pengamal.kel_id', $request->id_kel);
        }

        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('layouts.component._action', [
                    'model' => $model,
                    'title' => $model->name,
                    'url_show' => route('kk.show', $model->id_kk),
                    'url_edit' => route('kk.edit', $model->id_kk),
                    'url_destroy' => route('kk.destroy', $model->id_kk)
                ]);
            })
            ->addIndexColumn()
            ->setRowData([
                'provinsi' => function($model) {
                    return Provinsi::where('id', $model->prov_id)->first()->nama;
                },
                'kabupaten' => function($model) {
                    return Kabupaten::where('id', $model->kab_id)->first()->nama;
                },
                'kecamatan' => function($model) {
                    return Kecamatan::where('id', $model->kec_id)->first()->nama;
                },
                'kelurahan' => function($model) {
                    return Kelurahan::where('id', $model->kel_id)->first()->nama;
                },
            ])
            ->rawColumns(['action'])
            ->make(true);
    }

    public function getPengamalList(Request $request) {
        $limit = 20;
        $query = $request->q;
        $page =  $request->page;
        $start = ($page - 1) * $limit;
        $retVal = array();

        $modal = Pengamal::select('id' , 'name' , 'nik')
                    ->where('name' , 'like','%'.$query.'%')
                    ->orWhere('nik' , 'like' , '%'.$query.'%');
        $jumlah = $modal->count();
        $res = $modal->limit($limit)->offset($start)->get();
        
        $retVal['total_count'] = $jumlah;
        $retVal['incomplete_results'] = false;
        $retVal['items'] = $res;
        return response()->json($retVal);
    }

    private function _getFilterLocation($path_component) {
        $admin = Auth::user();
        $role = config('global.role')[$admin->role];
        $provinsi = Provinsi::where('id', $admin->prov_id)->first();
        $kabupaten = Kabupaten::where('id', $admin->kab_id)->first();
        $kecamatan = Kecamatan::where('id', $admin->kec_id)->first();
        $kelurahan = Kelurahan::where('id', $admin->kel_id)->first();
        $arr_prov = $this->_arrSelect(provinsi::all()->pluck('nama', 'id'), 'Pilih Provinsi');
        return view($path_component, [
            'model' => $admin,
            'role' => $role,
            'arr_prov' => $arr_prov,
            'provinsi' => $provinsi,
            'kabupaten' => $kabupaten,
            'kecamatan' => $kecamatan,
            'kelurahan' => $kelurahan,

        ]);
    }

    private function _arrSelect($model, $selected) {
        $arr = ['' => $selected];
        foreach ($model as $key => $value) {
            $arr[$key] = $value;
        }

        return $arr;
    }
}
