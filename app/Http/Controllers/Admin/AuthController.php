<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;

use App\User;

class AuthController extends Controller
{
    public function login() {
    	if(Auth::check()) {
    		return redirect('/');
    	}
    	return view('auth.login');
    }

    public function processLogin(Request $request) {
    	// return response()->json($request->all());
    	$request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

		// cek uesr
        $user = User::where('email', $request->email)->first();
        if($user) {
            if($user->is_active == 0) {
                return redirect('/login')->with('message_failed' , 'Akun anda belum aktif!');
            }
        } else {
	        return redirect('/login')->with('message_failed' , 'Email belum terdaftar!');
        }
        
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect('/');
        }

        // login failed
        return redirect('/login')->with('message_failed' , 'Login Gagal! Email atau Password salah!');
    }

    public function logout() {
		Auth::logout();
		return redirect('/login');
    }
}
