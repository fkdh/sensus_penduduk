<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DataTables;
use Auth;
use DB;

use GlobalHelper;

use App\Pengamal;
use App\Provinsi;
use App\Kabupaten;
use App\Kecamatan;
use App\Kelurahan;

class CountPengamalController extends Controller
{
    public function index()
    {
        $minAge = ['' => 'minimal umur'];
        $maxAge = ['' => 'maksimal umur'];
        for ($i=0; $i < 101; $i++) { 
             $minAge[$i] = $i;
             $maxAge[$i] = $i;
         }

        $filter_location = $this->_getFilterLocation('layouts.component._filter_location');
        return view('count-pengamal.list', compact('filter_location', 'minAge', 'maxAge'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dataTable(Request $request) {
        $filter_location = 'provinsi';
        $model = Pengamal::rightJoin('provinsi', 'pengamal.prov_id', '=', 'provinsi.id')
                ->groupBy('provinsi.id')
                ->select(DB::raw('count(pengamal.id) as total_count, provinsi.nama as region'));
        // return response()->json($request);
        
        if(!empty($request->id_prov)) {
            $filter_location = 'kabupaten';
            $model = Pengamal::rightJoin('kabupaten', 'pengamal.kab_id', '=', 'kabupaten.id')
                ->where('pengamal.prov_id', $request->id_prov)
                ->groupBy('kabupaten.id')
                ->select(DB::raw('count(pengamal.id) as total_count, kabupaten.nama as region'));
        }

        if(!empty($request->id_kab)) {
            $filter_location = 'kecamatan';
            $model = Pengamal::rightJoin('kecamatan', 'pengamal.kec_id', '=', 'kecamatan.id')
                ->where('pengamal.kab_id', $request->id_kab)
                ->groupBy('kecamatan.id')
                ->select(DB::raw('count(pengamal.id) as total_count, kecamatan.nama as region'));
        }

        if(!empty($request->id_kec)) {
            $filter_location = 'kelurahan';
            $model = Pengamal::rightJoin('kelurahan', 'pengamal.kel_id', '=', 'kelurahan.id')
                ->where('pengamal.kec_id', $request->id_kec)
                ->groupBy('kelurahan.id')
                ->select(DB::raw('count(pengamal.id) as total_count, kelurahan.nama as region'));
        }

        if(!empty($request->id_kel)) {
            $filter_location = 'kelurahan';
            $model = Pengamal::rightJoin('kelurahan', 'pengamal.kel_id', '=', 'kelurahan.id')
                ->where('pengamal.kel_id', $request->id_kel)
                ->groupBy('kelurahan.id')
                ->select(DB::raw('count(pengamal.id) as total_count, kelurahan.nama as region'));
        }

        if($request->min_age !== null && $request->max_age !== null) {
            // return response()->json('run filter age');
            $min_age = $request->min_age;
            $max_age = $request->max_age;
            $dateRange = GlobalHelper::rangeDateAge($min_age, $max_age);
            $minDate = $dateRange['min-date'];
            $maxDate = $dateRange['max-date'];

            $model->whereBetween('pengamal.birth_date', [$minDate, $maxDate]);
        }

        if(!empty($request->search_loaction)) {
            $model->where($filter_location.'.nama', 'like', '%'.$request->search_loaction.'%');
        }

        return DataTables::of($model)
            ->addIndexColumn()
            // ->setRowData([
            //     'count_pengamal' => function($model) use($model_query) {
            //         return Pengamal::where($model_query, $model->id)->count();
            //     }
            // ])
            ->make(true);

    }

    private function _getFilterLocation($path_component) {
        $admin = Auth::user();
        $role = config('global.role')[$admin->role];
        $provinsi = Provinsi::where('id', $admin->prov_id)->first();
        $kabupaten = Kabupaten::where('id', $admin->kab_id)->first();
        $kecamatan = Kecamatan::where('id', $admin->kec_id)->first();
        $kelurahan = Kelurahan::where('id', $admin->kel_id)->first();
        $arr_prov = $this->_arrSelect(provinsi::all()->pluck('nama', 'id'), 'Pilih Provinsi');
        return view($path_component, [
            'model' => $admin,
            'role' => $role,
            'arr_prov' => $arr_prov,
            'provinsi' => $provinsi,
            'kabupaten' => $kabupaten,
            'kecamatan' => $kecamatan,
            'kelurahan' => $kelurahan,

        ]);
    }

    private function _arrSelect($model, $selected) {
        $arr = ['' => $selected];
        foreach ($model as $key => $value) {
            $arr[$key] = $value;
        }

        return $arr;
    }
}
