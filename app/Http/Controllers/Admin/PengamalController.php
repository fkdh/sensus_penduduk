<?php

namespace App\Http\Controllers\Admin;

use App\Provinsi;
use App\Kabupaten;
use App\Kecamatan;
use App\Kelurahan;
use App\Pengamal;
use App\Job;
use App\Education;
use App\Salary;
use App\AgeRange;

use GlobalHelper;

use App\Http\Resources\PengamalResource;

use DataTables;
use Faker\Factory as Faker;
use DB;
use Carbon\Carbon;
use File;
use Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PengamalController extends Controller
{
    public function index()
    {
        $minAge = ['' => 'minimal umur'];
        $maxAge = ['' => 'maksimal umur'];
        for ($i=0; $i < 101; $i++) { 
             $minAge[$i] = $i;
             $maxAge[$i] = $i;
         }

        $filter_location = $this->_getFilterLocation('layouts.component._filter_location');
        // return $filter_location;

        $arr_job = $this->_arrSelect(Job::all()->pluck('job', 'id'), 'Pilih Pekerjaan');
        $arr_edu = $this->_arrSelect(Education::all()->pluck('education', 'id'), 'Pilih Pendidikan');
        $arr_salary = $this->_arrSelect(Salary::all()->pluck('name', 'id'), 'Pilih Penghasilan');
        return view('pengamal.list', compact('arr_job', 'arr_edu', 'minAge', 'maxAge', 'arr_salary', 'filter_location'));
    }

    private function _arrSelect($model, $selected) {
        $arr = ['' => $selected];
        foreach ($model as $key => $value) {
            $arr[$key] = $value;
        }

        return $arr;
    }

    private function _getFilterLocation($path_component, $prov_id = '') {
        $admin = Auth::user();
        $role = config('global.role')[$admin->role];
        $provinsi = Provinsi::where('id', $admin->prov_id)->first();
        $kabupaten = Kabupaten::where('id', $admin->kab_id)->first();
        $kecamatan = Kecamatan::where('id', $admin->kec_id)->first();
        $kelurahan = Kelurahan::where('id', $admin->kel_id)->first();
        $arr_prov = $this->_arrSelect(provinsi::all()->pluck('nama', 'id'), 'Pilih Provinsi');
        return view($path_component, [
            'model' => $admin,
            'role' => $role,
            'arr_prov' => $arr_prov,
            'provinsi' => $provinsi,
            'kabupaten' => $kabupaten,
            'kecamatan' => $kecamatan,
            'kelurahan' => $kelurahan,
            'prov_id' => $prov_id,

        ]);
    }

    public function create()
    {
        $model = New Pengamal;

        $arr_job = $this->_arrSelect(Job::all()->pluck('job', 'id'), 'Pilih Pekerjaan');
        $arr_edu = $this->_arrSelect(Education::all()->pluck('education', 'id'), 'Pilih Pendidikan');
        $arr_salary = $this->_arrSelect(Salary::all()->pluck('name', 'id'), 'Pilih Penghasilan');
        // $arr_prov = $this->_arrSelect(provinsi::all()->pluck('nama', 'id'), 'Pilih Provinsi');
        $form_location = $this->_getFilterLocation('layouts.component._form_location');

        return view('pengamal.form', compact('model', 'arr_job', 'arr_edu','arr_salary', 'form_location'));
    }

    private function _validator($data, $id = '') {
        return  Validator::make($data, [
            // 'address' => 'required',
            'blood' => 'required',
            'birth_date' => 'required',
            'education_id' => 'required',
            'gender' => 'required',
            'job_id' => 'required',
            'kab_id' => 'required',
            'kec_id' => 'required',
            'kel_id' => 'required',
            'married' => 'required',
            'name' => 'required|string',
            'nik' => 'required|numeric|unique:pengamal,nik,'.$id,
            'prov_id' => 'required',
            'ktp_path_send' => 'required',
        ], [
            'required' => ':attribute tidak boleh kosong',
            'unique' => ':attribute sudah terdaftar',
            'numeric' => ':attribute harus berupa angka',
        ], [
            // 'address' => 'Alamat',
            'blood' => 'Gol. Darah',
            'birth_date' => 'Tanggal Lahir',
            'education_id' => 'Pendidikan',
            'gender' => 'Jenis Kelamin',
            'job_id' => 'Pekerjaan',
            'prov_id' => 'Provinsi',
            'kab_id' => 'Kabupaten',
            'kec_id' => 'Kecamatan',
            'kel_id' => 'Kelurahan',
            'married' => 'Status',
            'name' => 'Nama',
            'nik' => 'NIK',
            'ktp_path_send' => 'KTP harus di isi',
        ])->validate();
    }


    public function store(Request $request)
    {
        $validated = $this->_validator($request->all());
        $validated['salary_id'] = $request->salary_id;
        unset($validated['ktp_path_send']);

        if($request->upload_ktp) {
            $img_path = GlobalHelper::uploadImage('ktp',$request->upload_ktp);
            $validated['ktp_path'] = $img_path;
        }

        $model = Pengamal::create($validated);
        return $model;
    }

    public function show($id)
    {
        //
    }

    public function setDie($id)
    {
        $model = Pengamal::findOrFail($id);
        return view('pengamal.set_die', compact('model'));
    }

    public function storeDie(Request $request, $id)
    {
        $model = Pengamal::findOrFail($id);
        $data['die_date'] = $request->die_date;
        $data['has_die'] = 1;
        return $model->update($data);
    }

    public function setAlive($id)
    {
        $model = Pengamal::findOrFail($id);
        return view('pengamal.set_alive', compact('model'));
    }

    public function storeAlive(Request $request, $id)
    {
        $model = Pengamal::findOrFail($id);
        $data['has_die'] = $request->has_die;
        $data['die_date'] = null;
        return $model->update($data);
    }

    public function edit($id)
    {
        $model = Pengamal::findOrFail($id);
        $prov_id = $model->prov_id;

        $arr_job = $this->_arrSelect(Job::all()->pluck('job', 'id'), 'Pilih Pekerjaan');
        $arr_edu = $this->_arrSelect(Education::all()->pluck('education', 'id'), 'Pilih Pendidikan');
        $arr_salary = $this->_arrSelect(Salary::all()->pluck('name', 'id'), 'Pilih Penghasilan');

        $form_location = $this->_getFilterLocation('layouts.component._form_location', $prov_id);

        return view('pengamal.form', compact('model', 'arr_job', 'arr_edu','arr_salary', 'form_location'));
    }

    public function update(Request $request, $id)
    {
        $model = Pengamal::findOrFail($id);

        $validated = $this->_validator($request->all(), $id);
        $validated['salary_id'] = $request->salary_id;
        unset($validated['ktp_path_send']);

        if($request->upload_ktp) {
            $path_delete = $model->ktp_path;
            $img_path = GlobalHelper::uploadImage('ktp',$request->upload_ktp, $path_delete);
            $validated['ktp_path'] = $img_path;
        }

        return $model->update($validated);
    }

    public function destroy($id)
    {
        $model = Pengamal::findOrFail($id);
        return $model->delete();
    }

    public function dataTable(Request $request) {
        $model = Pengamal::with(['job', 'education', 'provinsi', 'kabupaten', 'kecamatan', 'kelurahan', 'salary']);

        // return response()->json($request);
        // 
        // return response()->json($request->min_age !== 'x');
        
        if(!empty($request->id_prov)) {
            $model->where('prov_id', $request->id_prov);
        } 

        if(!empty($request->id_kab)) {
            $model->where('kab_id', $request->id_kab);
        } 

        if(!empty($request->id_kec)) {
            $model->where('kec_id', $request->id_kec);
        } 

        if(!empty($request->id_kel)) {
            $model->where('kel_id', $request->id_kel);
        }

        if(!empty($request->education_id)) {
            $model->where('education_id', $request->education_id);
        }

        if(!empty($request->job_id)) {
            $model->where('job_id', $request->job_id);
        }

        if(!empty($request->salary_id)) {
            $model->where('salary_id', $request->salary_id);
        }

        if(!empty($request->gender)) {
            $model->where('gender', $request->gender);
        }

        if(!empty($request->blood)) {
            $model->where('blood', $request->blood);
        }

        if(!empty($request->married)) {
            $model->where('married', $request->married);
        }

        if($request->has_die !== null) {
            $model->where('has_die', $request->has_die);
        }

        if($request->min_age !== null && $request->max_age !== null) {
            // return response()->json('run filter age');
            $min_age = $request->min_age;
            $max_age = $request->max_age;
            $dateRange = GlobalHelper::rangeDateAge($min_age, $max_age);
            $minDate = $dateRange['min-date'];
            $maxDate = $dateRange['max-date'];

            $model->whereBetween('birth_date', [$minDate, $maxDate]);
        }

        if(!empty($request->jamaah)) {
            $age_range = [];
            $gender = '';
            switch ($request->jamaah) {
                case 'Kanak-Kanak':
                    $age_range = AgeRange::where('name', 'Kanak-Kanak')->first();
                    break;
                case 'Remaja':
                    $age_range = AgeRange::where('name', 'Remaja')->first();
                    break;
                case 'Ibu-Ibu':
                    $age_range = AgeRange::where('name', 'Dewasa')->first();
                    $gender = 'Perempuan';
                    break;
                case 'Bapak-Bapak':
                    $gender = 'Laki-Laki';
                    $age_range = AgeRange::where('name', 'Dewasa')->first();
                    break;
                default:
                    break;
            }
            $min_age = $age_range->min_age;
            $max_age = $age_range->max_age;
            $dateRange = GlobalHelper::rangeDateAge($min_age, $max_age);
            $minDate = $dateRange['min-date'];
            $maxDate = $dateRange['max-date'];
            $model->whereBetween('birth_date', [$minDate, $maxDate]);
            // return response()->json($gender);
            if(!empty($gender)) {
                $model->where('gender', $gender);
            }
        }

        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                $action = [
                    'model' => $model,
                    'title' => $model->name,
                    'url_edit' => route('pengamal.edit', $model->id),
                    'url_destroy' => route('pengamal.destroy', $model->id),
                    'url_set_die' => route('pengamal.set-die', $model->id),
                    // 'url_set_alive' => route('pengamal.set-alive', $model->id),
                ];

                if($model->has_die == 1) {
                    $action['url_set_alive'] = route('pengamal.set-alive', $model->id);
                }
                return view('layouts.component._action',$action);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->setRowData([
                'age' => function($model) {
                    return Carbon::parse($model->birth_date)->age;
                },
                'has_die' => function($model) {
                    return $model->has_die == 0 ? 'Masih Hidup' : 'Telah Meninggal';
                },
                'jamaah' => function($model) {
                    $jamaah = '';
                    $age = Carbon::parse($model->birth_date)->age;
                    $jamaah = AgeRange::where('min_age', '<=', $age)->where('max_age', '>=', $age)->first()->name;
                    if($jamaah == 'Dewasa') {
                        if($model->gender == 'Perempuan') {$jamaah="Ibu-Ibu";} else {$jamaah="Bapak-Bapak";}
                    }
                    return $jamaah;
                },
            ])
            ->make(true);
    }

    public function getRegion(Request $request) {
        if($request->get_by == 'provinsi') {
            return $this->_getProvinsi($request);
        }
        if($request->get_by == 'kabupaten') {
            return $this->_getKabupaten($request);
        }
        if($request->get_by == 'kecamatan') {
            return $this->_getKecamatan($request);
        }
        if($request->get_by == 'kelurahan') {
            return $this->_getKelurahan($request);
        }
        return response()->json($request);

    }

    private function _getProvinsi($request) {
        $is_selected = $request->is_selected;
        $provinsi = Provinsi::orderBy('nama', 'asc')->get();

        $str = '<option value="">pilih Provinsi</option>';

        foreach ($provinsi as $value) {
            if(!$is_selected == null && $is_selected == $value['id']) {
                $str.= '<option value="'.$value['id'].'" selected>'. $value['nama'].'</option>';
            } else {
                $str.= '<option value="'.$value['id'].'" >'. $value['nama'].'</option>';
            }
        }
        return $str;
    }

    private function _getKabupaten($request) {
        $is_selected = $request->is_selected;
        $region_id = $request->region_id;
        $kabupaten = Kabupaten::where('id_prov', $region_id)->orderBy('nama', 'asc')->get();

        $str = '<option value="">pilih Kabupaten</option>';

        foreach ($kabupaten as $value) {
            if(!$is_selected == null && $is_selected == $value['id']) {
                $str.= '<option value="'.$value['id'].'" selected>'. $value['nama'].'</option>';
            } else {
                $str.= '<option value="'.$value['id'].'" >'. $value['nama'].'</option>';
            }
        }
        return $str;
    }

    private function _getKecamatan($request) {
        $is_selected = $request->is_selected;
        $region_id = $request->region_id;
        $kecamatan = Kecamatan::where('id_kab', $region_id)->orderBy('nama', 'asc')->get();

        $str = '<option value="">pilih Kecamatan</option>';

        foreach ($kecamatan as $value) {
            if(!$is_selected == null && $is_selected == $value['id']) {
                $str.= '<option value="'.$value['id'].'" selected>'. $value['nama'].'</option>';
            } else {
                $str.= '<option value="'.$value['id'].'" >'. $value['nama'].'</option>';
            }
        }
        return $str;
    }

    private function _getKelurahan($request) {
        $is_selected = $request->is_selected;
        $region_id = $request->region_id;
        $kelurahan = Kelurahan::where('id_kec', $region_id)->orderBy('nama', 'asc')->get();

        $str = '<option value="">pilih Kelurahan</option>';

        foreach ($kelurahan as $value) {
            if(!$is_selected == null && $is_selected == $value['id']) {
                $str.= '<option value="'.$value['id'].'" selected>'. $value['nama'].'</option>';
            } else {
                $str.= '<option value="'.$value['id'].'" >'. $value['nama'].'</option>';
            }
        }
        return $str;
    }
}
