<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use DataTables;

use App\KkDetail;

class KkDetailController extends Controller
{
    public function index()
    {
        //
    }

    private function _validator($data, $custom_validated = 'required') {
        return  Validator::make($data, [
            'id_kk' => 'required',
            'id_pengamal' => $custom_validated,
            'user_as' => 'required',
        ], [
            'required' => ':attribute tidak boleh kosong',
            'unique' => ':attribute sudah terdaftar di Keluarga ini',
        ], [
            'id_pengamal' => 'Nama / NIK',
            'user_as' => 'Status Keluarga',
        ])->validate();
    }

    public function create($id)
    {
        $model = New KkDetail;
        $id_kk = $id;
        return view('kk_detail.form', compact('id_kk', 'model'));
    }

    public function store(Request $request)
    {
        // cekBy kkDetail in this family
        $user = KkDetail::where('id_kk', $request->id_kk)->where('id_pengamal', $request->id_pengamal)->first();

        // return response()->json($user);

        if(empty($user)) {
            // return response()->json('belum terdaftar di family ini');
            $validated = $this->_validator($request->all());
            $model = KkDetail::create($validated);
            return $model;
        }

        $custom_validated = 'required|unique:kk_detail,id_pengamal';
        $validated = $this->_validator($request->all(), $custom_validated);

        $model = KkDetail::create($validated);
        return $model;
    }

    public function show($id)
    {
        $model = KkDetail::findOrFail($id);
        return view('kk_detail.form', compact('model'));
    }

    public function edit($id)
    {
        $model = KkDetail::findOrFail($id);
        return view('kk_detail.form', compact('model'));
    }

    public function update(Request $request, $id)
    {
        $validated = $this->validate($request, [
            'user_as' => 'required',
        ]);

        $model = KkDetail::findOrFail($id);
        $model->update($validated);

        return $model;
    }

    public function destroy($id)
    {
        $model = KkDetail::findOrFail($id);
        $model->delete();
    }

    public function dataTable(Request $request) {
        $model = KkDetail::join('pengamal', 'pengamal.id', '=', 'kk_detail.id_pengamal')
                ->select('kk_detail.id as id_kk_detail','kk_detail.user_as','pengamal.*');

        if(!empty($id_kk = $request->id_kk)) {
            $model->where('kk_detail.id_kk', $id_kk);
        }

        return DataTables::of($model->get())
            ->addColumn('action', function ($model) {
                $action = [
                    'model' => $model,
                    'title' => $model->name,
                    // 'url_show' => route('kk-detail.show', $model->id_kk_detail),
                ];

                if($model->user_as !== 'Ayah') {
                    $action['url_destroy'] = route('kk-detail.destroy', $model->id_kk_detail);
                    $action['url_edit'] = route('kk-detail.edit', $model->id_kk_detail);
                }

                return view('layouts.component._action', $action);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}
