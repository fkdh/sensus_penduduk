<?php

namespace App\Http\Controllers\Admin\MasterData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use App\Role;

class RoleController extends Controller
{
    public function index()
    {
        return view('master-data.role.list');
    }

    public function create()
    {
        $model = New Role;

        return view('master-data.role.form', compact('model'));
    }

    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            'role' => 'required',
        ]);

        $model = Role::create($validated);
        return $model;
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        $model = Role::findOrFail($id);

        return view('master-data.role.form', compact('model'));
    }

    public function update(Request $request, $id)
    {
        $validated = $this->validate($request, [
            'role' => 'required',
        ]);

        $model = Role::findOrFail($id);
        $model->update($validated);
        return $model;
    }

    public function destroy($id)
    {
        $model = Role::findOrFail($id);
        $model->delete();
    }

    public function dataTable(Request $request) {
        $model = Role::query();
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('layouts.component._action', [
                    'model' => $model,
                    'title' => $model->role,
                    'url_edit' => route('role.edit', $model->id),
                    'url_destroy' => route('role.destroy', $model->id)
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}
