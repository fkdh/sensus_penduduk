<?php

namespace App\Http\Controllers\Admin\MasterData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DataTables;

use App\AgeRange;


class AgeRangeController extends Controller
{
    public function index()
    {
        return view('master-data.age-range.list');
    }

    public function create()
    {
        $model = New AgeRange;
        return view('master-data.age-range.form', compact('model'));
    }

    private function _validator($data, $id = '') {
        return  Validator::make($data, [
            'name' => 'required|string',
            'min_age' => 'required|numeric',
            'max_age' => 'required|numeric|gt:min_age',
        ], [
            'required' => ':attribute tidak boleh kosong',
            'numeric' => ':attribute harus berupa angka',
            'gt' => ':attribute tidak boleh lebih kecil dari umur minimal',
        ], [
            'name' => 'Nama',
            'min_age' => 'Umur minimal',
            'max_age' => 'Umur maksimal',
        ])->validate();
    }


    public function store(Request $request)
    {
        $validated = $this->_validator($request->all());
        return $model = AgeRange::create($validated);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $model = AgeRange::findOrFail($id);
        return view('master-data.age-range.form', compact('model'));
    }

    public function update(Request $request, $id)
    {
        $validated = $this->_validator($request->all());

        $model = AgeRange::findOrFail($id);
        return $model->update($validated);
    }

    public function destroy($id)
    {
        $model = AgeRange::findOrFail($id);
        return $model->delete();
    }

    public function dataTable(Request $request) {
        $model = AgeRange::query();

        // $model = AgeRange::all();
        // return response()->json($model);
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('layouts.component._action', [
                    'model' => $model,
                    'title' => $model->name,
                    'url_edit' => route('age-range.edit', $model->id),
                    'url_destroy' => route('age-range.destroy', $model->id)
                ]);
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}
