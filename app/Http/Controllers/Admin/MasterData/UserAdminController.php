<?php

namespace App\Http\Controllers\Admin\MasterData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use DataTables;

use App\User;
use App\Role;
use App\Provinsi;
use App\Kabupaten;
use App\Kecamatan;
use App\Kelurahan;

class UserAdminController extends Controller
{
    public function index()
    {
        return view('master-data.admin.list');
    }

    public function create()
    {
        $model = New User;
        $role = Role::all()->pluck('role', 'id');
        $arr_prov = $this->_arrSelect(provinsi::all()->pluck('nama', 'id'), 'Pilih Provinsi');
        // dd($model);
        return view('master-data.admin.form', compact('model', 'role', 'arr_prov'));
    }

    private function _validator($data, $id = '',$required_pas = '') {
        return  Validator::make($data, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'confirmed'.$required_pas,
            'role' => 'required',
            'is_active' => 'required',
            'phone' => 'numeric',
            'prov_id' => '',
            'kab_id' => '',
            'kec_id' => '',
            'kel_id' => '',
        ], [
            'required' => ':attribute tidak boleh kosong',
            'unique' => ':attribute sudah terdaftar',
            'confirmed' => ':attribute harus sama',
            'email' => 'silahkan sesuaikan dengan format email. - Contoh: contoh@gmail.com',
        ], [
            'name' => 'Nama',
        ])->validate();
    }

    public function store(Request $request)
    {
        $required_pas = '|required|string';
        $validated = $this->_validator($request->all(),'', $required_pas);

        $validated['password'] = Hash::make($validated['password']);

        return $model = User::create($validated);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $model = User::findOrFail($id);
        $role = Role::all()->pluck('role', 'id');
        $arr_prov = $this->_arrSelect(provinsi::all()->pluck('nama', 'id'), 'Pilih Provinsi');
        return view('master-data.admin.form', compact('model', 'role', 'arr_prov'));
    }

    public function update(Request $request, $id)
    {
        $validated = $this->_validator($request->all(), $id);

        $validated['password'] = Hash::make($validated['password']);

        $model = User::findOrFail($id);
        return $model->update($validated);
    }

    public function destroy($id)
    {
        $model = User::findOrFail($id);
        $model->delete();
    }

    public function dataTable(Request $request) {
        $is_active = config('select.is_active');
        // return response()->json($is_active[1]);
        $model = User::query();
        return DataTables::of($model)
            ->addColumn('action', function ($model) {
                return view('layouts.component._action', [
                    'model' => $model,
                    'title' => $model->name,
                    'url_edit' => route('admin.edit', $model->id),
                    'url_destroy' => route('admin.destroy', $model->id)
                ]);
            })
            ->setRowData([
                'role' => function($model) {
                    $role = Role::find($model->role);
                    return $role->role;
                },
                'is_active' => function($model) {
                    $is_active = config('select.is_active');
                    return $is_active[$model->is_active];
                },
            ])
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    private function _arrSelect($model, $selected) {
        $arr = ['' => $selected];
        foreach ($model as $key => $value) {
            $arr[$key] = $value;
        }

        return $arr;
    }
}
