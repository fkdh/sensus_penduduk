<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PengamalResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'nik' => $this->nik,
            'birth_date' => $this->birth_date,
            'provinsi' => $this->provinsi['nama'],
            'kabuptaen' => $this->kabupaten['nama'],
            'kecamatan' => $this->kecamatan['nama'],
            'kelurahan' => $this->kelurahan['nama'],
            'address' => $this->address,
            'job' => $this->job->job,
            'education' => $this->education->education,
            'blood' => $this->blood,
            'gender' => $this->gender,
            'married' => $this->married,
            'has_die' => $this->has_die,
            'die_date' => $this->die_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
