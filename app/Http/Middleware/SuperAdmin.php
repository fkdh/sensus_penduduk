<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;
use Request;
use Auth;

class SuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->role == 1){
            return $next($request);     
        }

        return Redirect::to('/');
        // return $next($request);
    }
}
