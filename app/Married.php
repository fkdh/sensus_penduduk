<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Married extends Model
{
    protected $guarded = [];
    protected $table = 'married';
}
