<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    protected $guarded = [];
    protected $table = 'education';

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
