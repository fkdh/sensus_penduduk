<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blood extends Model
{
    protected $guarded = [];
    protected $table = 'blood';
}
