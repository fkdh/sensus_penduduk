<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kk extends Model
{
    protected $guarded = [];
    protected $table = 'kk';

    public function pengamal() {
    	return $this->belongsTo('App\Pengamal', 'id_pengamal', 'id');
    }

    public function kk_detail()
    {
        return $this->hasMany('App\KkDetail', 'id_kk', 'id');
    }
}
