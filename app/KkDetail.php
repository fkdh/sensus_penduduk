<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KkDetail extends Model
{
    protected $guarded = [];
    protected $table = 'kk_detail';
}
