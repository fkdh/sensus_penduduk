<?php
namespace App\Helpers;
use Carbon\Carbon;
use App\Pengamal;

use File;

class GlobalHelper {
	public static function rangeDateAge($min_age, $max_age) {
                $maxDate = Carbon::now()->subYears($min_age)->format('Y-m-d');
                $minDate = Carbon::now()->subYears($max_age +1)->addDay()->format('Y-m-d');
                return [
                	'min-date' => $minDate,
                	'max-date' => $maxDate
                ];
	}

        public static function uploadImage($path, $img, $delete_path = '') {
                if(!empty($delete_path)) {
                    self::deleteImage($delete_path);
                }
                $image_data = $img;
                $image_array_1 = explode(";", $image_data);
                $image_array_2 = explode(",", $image_array_1[1]);
                $data = base64_decode($image_array_2[1]);
                $image_name = time() . '.png';
                $upload_path = public_path('upload/img/'.$path.'/' . $image_name);
                file_put_contents($upload_path, $data);
                return '/upload/img/'.$path.'/'.$image_name;
        }

        static function deleteImage($path){
                $image_path = public_path($path);
                if (File::exists($image_path)) {
                    if (File::delete($image_path)) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
}