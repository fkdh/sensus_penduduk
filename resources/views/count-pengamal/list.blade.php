@extends('layouts.master')

@section('title') 
  Jumlah Pengamal
@endsection

@section('head_title') 
  Jumlah Pengamal
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">Jumlah Pengamal</a></li>
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Filter</h3>

        <div class="card-tools">
          {{-- <a href="{{route('role.create')}}" class="btn btn-success btn-sm modal-show" title="Tambah Role"><i class="fas fa-plus"></i> Tambah</a> --}}
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button> --}}
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="form-group row filter-location">
          {!! $filter_location !!}
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {!! Form::dropdownRaw('min_age' , 'Min Age' , $minAge) !!}
            </div>
            <div class="col-md-2">
                {!! Form::dropdownRaw('max_age' , 'Max Age' , $maxAge) !!}
            </div>
            <div class="col-md-2">
                {!! Form::textRaw('search_loaction' , 'Cari Lokasi') !!}
            </div>
        </div>
      </div>
      <!-- /.card-body -->
    </div>
  </div>
</div>
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Data</h3>

        <div class="card-tools">
          {{-- <a href="{{route('role.create')}}" class="btn btn-success btn-sm modal-show" title="Tambah Role"><i class="fas fa-plus"></i> Tambah</a> --}}
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button> --}}
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        {{-- Table Field --}}
        <table id="datatable" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Region</th>
              <th>Count</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
  </div>
</div>
</div>
@endsection('')


@section('script')
<script>
  var csrf_token = $('meta[name="csrf-token"]').attr('content');
  var table
  var arr_select = ['min_age','max_age']
  var arr_select_2 = ['select_provinsi','select_kabupaten','select_kecamatan','select_kelurahan','min_age','max_age']

  var select_provinsi = $('#select_provinsi')
  var select_kabupaten = $('#select_kabupaten')
  var select_kecamatan = $('#select_kecamatan')
  var select_kelurahan = $('#select_kelurahan')

  $(document).ready(function() {
    arr_select_2.forEach(function (key, value) {
        $('#' + key).select2()
    });

    getRegion('kabupaten', select_provinsi.children("option:selected").val(), select_kabupaten.children("option:selected").val());
    getRegion('kecamatan', select_kabupaten.children("option:selected").val(), select_kecamatan.children("option:selected").val());
    getRegion('kelurahan', select_kecamatan.children("option:selected").val(), select_kelurahan.children("option:selected").val());

    table = $('#datatable').DataTable({
      dom: '<"html5buttons">iBflrtp',
        responsive: true,
        processing: true,
        serverSide: true,
        "lengthMenu": [ [-1], ["All"] ],
        // scrollX: true,
        ajax: {
          url : "{{ route('table.count.pengamal') }}",
          data : function(d) {
            d.id_prov = select_provinsi.children("option:selected").val();
            d.id_kab = select_kabupaten.children("option:selected").val();
            d.id_kec = select_kecamatan.children("option:selected").val();
            d.id_kel = select_kelurahan.children("option:selected").val();
            d.min_age = $('#min_age').children("option:selected").val();
            d.max_age = $('#max_age').children("option:selected").val();
            d.search_loaction = $('#search_loaction').val();
          }
        },
        columns: [
            {data: 'DT_RowIndex', name: 'region', searchable: 'false',orderable: 'false'},
            {data: 'region', name: 'region', searchable: 'false',orderable: 'false'},
            {data: 'total_count', name: 'total_count', searchable: 'false', orderable: 'true'}
        ]
    });

    $(document).on("change", "select[name='select_provinsi']" , function() {
        id_province = $(this).children("option:selected").val();
        select_kabupaten.html('<option value="">pilih Kabupaten</option>')
        select_kecamatan.html('<option value="">pilih Kecamatan</option>')
        select_kelurahan.html('<option value="">pilih Kelurahan</option>')
        getRegion('kabupaten', id_province);
        table.draw();
    });

    $(document).on("change", "select[name='select_kabupaten']" , function() {
        id_kabupaten = $(this).children("option:selected").val();
        select_kecamatan.html('<option value="">pilih Kecamatan</option>')
        select_kelurahan.html('<option value="">pilih Kelurahan</option>')
        getRegion('kecamatan', id_kabupaten);
        table.draw();
    });

    $(document).on("change", "select[name='select_kecamatan']" , function() {
        id_kecamatan = $(this).children("option:selected").val();
        select_kelurahan.html('<option value="">pilih Kelurahan</option>')
        getRegion('kelurahan', id_kecamatan);
        table.draw();
    });

    $('#search_loaction').keyup(function(e) {
      table.draw();
    })

    $(document).on("change", "#search_loaction" , function() {
        console.log(this.val)
    });

    arr_select.forEach(function (key, value) {
        $('#' + key).on('change', function(e) {
          cekAge = checkAge()
          if(cekAge == 0) {return}
          console.log('run tes')
          table.draw();
        })
    });

    function checkAge() {
      console.log('tes cek age')
      var min_age = parseInt($('#min_age').val())
      var max_age = parseInt($('#max_age').val())

      if(max_age < min_age && min_age != null && max_age != null) {
        alert('max age tidak boleh lebih kecil')
        return 0;
      }
    }

    function getRegion($get_by, $region_id, $is_selected = '') {
      $.ajax({
        url: "{{route('get.region')}}",
        method: "POST",
        "data": {
          '_token' : csrf_token,
          'get_by' : $get_by,
          'region_id' : $region_id,
          'is_selected' : $is_selected,
        },
        success: function(res) {
          if($get_by == 'provinsi') {
            select_provinsi.html(res)
          }
          if($get_by == 'kabupaten') {
            select_kabupaten.html(res)
          }
          if($get_by == 'kecamatan') {
            select_kecamatan.html(res)
          }
          if($get_by == 'kelurahan') {
            select_kelurahan.html(res)
          }
        }
      })
    }
  })
  
</script>
@endsection