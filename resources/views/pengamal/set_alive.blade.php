{!! Form::model($model, [
    'route' => ['pengamal.store-alive', $model->id],
    'method' => 'POST',
]) !!}

{!! Form::dropdownRaw('has_die' , 'Set Hidup', ['1' => 'Set Meninggal','0' => 'Set Hidup']) !!}

{!! Form::close() !!}