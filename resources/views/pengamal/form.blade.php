{!! Form::model($model, [
    'route' => $model->exists ? ['pengamal.update', $model->id] : 'pengamal.store',
    'method' => $model->exists ? 'PUT' : 'POST'
]) !!}

<input type="hidden" name="url-get-region" value="{{route('get.region')}}">
<input type="hidden" name="upload_ktp" id="upload_ktp" value="">


{!! Form::textRaw('name' , 'Nama') !!}
{!! Form::textRaw('nik' , 'NIK') !!}
{!! Form::fileRaw('ktp_path' , 'KTP : ') !!}
<p class="text-bold">Maksimal foto 1 mb</p>
<input type="hidden" name="ktp_path_send" id="ktp_path_send" value="{{$model->ktp_path ? 'exist' : ''}}">
<div id="image-preview"></div>
{!! Form::textRaw('birth_date' , 'Tanggal Lahir') !!}
{{-- {!! Form::dropdownRaw('prov_id' , 'Provinsi', $arr_prov) !!}
{!! Form::dropdownRaw('kab_id' , 'Kabupaten/Kota', ['' => 'pilih Kabupaten/Kota']) !!}
{!! Form::dropdownRaw('kec_id' , 'Kecamatan', ['' => 'pilih Kecamatan']) !!}
{!! Form::dropdownRaw('kel_id' , 'Kelurahan', ['' => 'pilih Kelurahan']) !!} --}}
{{-- {!! Form::textRaw('address' , 'Alamat') !!} --}}
{!! $form_location !!}
{!! Form::dropdownRaw('education_id' , 'Pendidikan', $arr_edu) !!}
{!! Form::dropdownRaw('job_id' , 'Pekerjaan', $arr_job) !!}
{!! Form::dropdownRaw('salary_id' , 'Penghasilan', $arr_salary) !!}
{!! Form::dropdownRaw('blood' , 'Gol. Darah', config('select.blood')) !!}
{!! Form::dropdownRaw('gender' , 'Jenis Kelamin', config('select.gender')) !!}
{!! Form::dropdownRaw('married' , 'Status', config('select.married')) !!}

{!! Form::close() !!}

<script>
  var url_getRegion = $('input[name="url-get-region"]').val()
      id_province = '{{$model->prov_id}}'
      id_kabupaten = '{{$model->kab_id}}'
      id_kecamatan = '{{$model->kec_id}}'
      model = "{{$model}}"

      select_provinsi = $('#prov_id')
      selected_kab_id = '{{$model->kab_id}}'
      select_kabupaten = $('#kab_id')
      selected_kec_id = '{{$model->kec_id}}'
      select_kecamatan = $('#kec_id')
      selected_kel_id = '{{$model->kel_id}}'
      select_kelurahan = $('#kel_id')
      var image_crop = ''
      var reader = ''
      var img_path = "{{$model->ktp_path}}"
      var img_model = "{{asset($model->ktp_path)}}"

      arr_select_2 = ['prov_id', 'kab_id', 'kec_id', 'kel_id', 'job_id', 'salary_id', 'education_id', 'blood', 'gender', 'married'];

  $(document).ready(function() {
    console.log(img_model)
    reader = new FileReader();
    image_crop = $('#image-preview').croppie({
      enableExif:true,
      viewport:{
        width:323,
        height:204,
      },
      boundary:{
        width:'100%',
        height:204
      },
    });

    if(img_path !== '') {
      image_crop.croppie('bind', {
        url:img_model,
        points: [0,0]
      })
    }

    // image_crop.croppie('bind', {
    //   url:event.target.result
    // }

    function cek_img(img) {
      var size_max = 1024 * 1000
      var size_img = img.files[0].size
      var ext = $(img).val().split('.').pop().toLowerCase();
      // console.log(size_img)
      // console.log(size_max)
      // return 0
      if ($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
        alert('Image Format Harus JPG, JPEG, PNG or GIF.')
        return 0
      } else {
        if(size_img > size_max) {
          alert('File tidak boleh lebih dari 1 Mb')
          return 0
        }
      }
      return 1
    }

    $('#ktp_path').change(function(e){
      if(cek_img(this) == 0) {
        $('#ktp_path').val('');
        return
      } else {
        $('#ktp_path_send').val('exist')
      }
      reader.onload = function(event){
        // console.log(event.target.result)
        image_crop.croppie('bind', {
          url:event.target.result
        }).then(function(){
          console.log('jQuery bind complete');
        });
      }
      reader.readAsDataURL(this.files[0]);
    });

    $('#modal-btn-save').on('click', function() {
      image_crop.croppie('result', {
        type:'canvas',
        size:'original'
      }).then(function(response){
        $('#upload_ktp').val(response)
      });
    })

    if(model == "[]") {
      getRegion('kabupaten', select_provinsi.children("option:selected").val(), select_kabupaten.children("option:selected").val());
      getRegion('kecamatan', select_kabupaten.children("option:selected").val(), select_kecamatan.children("option:selected").val());
      getRegion('kelurahan', select_kecamatan.children("option:selected").val(), select_kelurahan.children("option:selected").val());
    } else {
      getRegion('kabupaten', id_province, selected_kab_id);
      getRegion('kecamatan', id_kabupaten, selected_kec_id);
      getRegion('kelurahan', id_kecamatan, selected_kel_id);
    }



    arr_select_2.forEach(function (key, value) {
        $('#' + key).select2()
    });

    $(function() {
      $('input[name="birth_date"]').daterangepicker({
        locale: {
            format: 'YYYY-MM-DD'
        },
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'),10)
      }, function(start, end, label) {
        var years = moment().diff(start, 'years');
        // alert("You are " + years + " years old!");
      });

    $(document).on("change", "#prov_id" , function() {
        id_province = $(this).children("option:selected").val();

        // blank
        $('#kab_id').html('<option value="">pilih Kabupaten</option>')
        $('#kec_id').html('<option value="">pilih Kecamatan</option>')
        $('#kel_id').html('<option value="">pilih Kelurahan</option>')

        select_kabupaten = $('#kab_id')
        getRegion('kabupaten', id_province, selected_kab_id);
    });

    $(document).on("change", "#kab_id" , function() {
        id_kabupaten = $(this).children("option:selected").val();

        // blank
        $('#kec_id').html('<option value="">pilih Kecamatan</option>')
        $('#kel_id').html('<option value="">pilih Kelurahan</option>')

        select_kecamatan = $('#kec_id')
        getRegion('kecamatan', id_kabupaten, selected_kec_id);
    });

    $(document).on("change", "#kec_id" , function() {
        id_kecamatan = $(this).children("option:selected").val();
        select_kelurahan = $('#kel_id')
        getRegion('kelurahan', id_kecamatan, selected_kab_id);
    });
  })

  function getRegion(get_by, region_id, is_selected = '') {
      $.ajax({
        url: "{{route('get.region')}}",
        method: "POST",
        "data": {
          '_token' : csrf_token,
          'get_by' : get_by,
          'region_id' : region_id,
          'is_selected' : is_selected,
        },
        success: function(res) {
          if(get_by == 'provinsi') {
            select_provinsi.html(res)
          }
          if(get_by == 'kabupaten') {
            select_kabupaten.html(res)
          }
          if(get_by == 'kecamatan') {
            select_kecamatan.html(res)
          }
          if(get_by == 'kelurahan') {
            select_kelurahan.html(res)
          }
        }
      })
    }
  });

</script>
