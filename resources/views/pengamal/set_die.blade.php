{!! Form::model($model, [
    'route' => ['pengamal.store-die', $model->id],
    'method' => 'POST',
]) !!}

{!! Form::textRaw('die_date' , 'Tanggal Meninggal Dunia') !!}

{!! Form::close() !!}

<script>
  $('input[name="die_date"]').daterangepicker({
    locale: {
        format: 'YYYY-MM-DD'
    },
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  }, function(start, end, label) {
    var years = moment().diff(start, 'years');
    // alert("You are " + years + " years old!");
  });
</script>
