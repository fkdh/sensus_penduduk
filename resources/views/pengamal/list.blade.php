@extends('layouts.master')

@section('title') 
  Pengamal
@endsection

@section('head_title') 
  Pengamal
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">Pengamal</a></li>
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
  <div class="col-12">
    <div class="card collapsed-card">
      <div class="card-header">
        <h3 class="card-title">Filter</h3>

        <div class="card-tools">
          {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button> --}}
            <a href="{{route('pengamal.create')}}" class="btn btn-success btn-sm modal-show" title="Tambah Pengamal"><i class="fas fa-plus"></i> Tambah</a>
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        {{-- Table Field --}}
        <div class="form-group row filter-location">
          {!! $filter_location !!}
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {!! Form::dropdownRaw('select_gender' , 'Jenis Kelamin' , config('select.gender')) !!}
            </div>
            <div class="col-md-2">
                {!! Form::dropdownRaw('select_blood' , 'Gol. Darah' , config('select.blood')) !!}
            </div>
            <div class="col-md-2">
                {!! Form::dropdownRaw('select_education' , 'Pendidikan' , $arr_edu) !!}
            </div>
            <div class="col-md-3">
                {!! Form::dropdownRaw('select_job' , 'Pekerjaan' , $arr_job) !!}
            </div>
            <div class="col-md-3">
                {!! Form::dropdownRaw('select_salary' , 'Pilih Penghasilan', $arr_salary) !!}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {!! Form::dropdownRaw('min_age' , 'Min Age' , $minAge) !!}
            </div>
            <div class="col-md-2">
                {!! Form::dropdownRaw('max_age' , 'Max Age' , $maxAge) !!}
            </div>
            <div class="col-md-2">
                {!! Form::dropdownRaw('jamaah' , 'Jamaah', config('select.jamaah')) !!}
            </div>
            <div class="col-md-2">
                {!! Form::dropdownRaw('select_status' , 'Status' , config('select.married')) !!}
            </div>
            <div class="col-md-2">
                {!! Form::dropdownRaw('select_is_alive' , 'is_alive' , config('select.has_die')) !!}
            </div>
        </div>
      </div>
      <!-- /.card-body -->
    </div>
  </div>
</div>
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Data</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button> --}}
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        {{-- Table Field --}}
        <table id="datatable" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>NIK</th>
              <th>Jenis Kelamin</th>
              <th>Gol. Darah</th>
              <th>Tanggal Lahir</th>
              <th>Umur</th>
              <th>Pendidikan</th>
              <th>Pekerjaan</th>
              <th>Penghaslian</th>
              <th>Status</th>
              <th>Jamaah</th>
              <th>Provinsi</th>
              <th>Kabupaten / Kota</th>
              <th>Kecamatan</th>
              <th>Desa</th>
              {{-- <th>Alamat</th> --}}
              <th>Status Hidup</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
  </div>
</div>
</div>
@endsection('')


@section('script')
<script>
  var csrf_token = $('meta[name="csrf-token"]').attr('content');
  var table

  var arr_select = ['select_gender',
                    'select_blood',
                    'select_education',
                    'select_job',
                    'select_status',
                    'select_is_alive',
                    'select_salary',
                    'min_age',
                    'max_age',
                    'jamaah']

  var arr_select_2 = ['select_provinsi',
                      'select_kabupaten',
                      'select_kecamatan',
                      'select_kelurahan',
                      'select_gender',
                      'select_blood',
                      'select_education',
                      'select_job',
                      'select_status',
                      'select_is_alive',
                      'select_salary',
                      'min_age',
                      'max_age',
                      'jamaah']

  $(document).ready(function() {
    var select_provinsi = $('select[name="select_provinsi"]')
    var select_kabupaten = $('select[name="select_kabupaten"]')
    var select_kecamatan = $('select[name="select_kecamatan"]')
    var select_kelurahan = $('select[name="select_kelurahan"]')

    arr_select_2.forEach(function (key, value) {
        $('#' + key).select2()
    });

    // getRegion('provinsi');
    
    getRegion('kabupaten', select_provinsi.children("option:selected").val(), select_kabupaten.children("option:selected").val());
    getRegion('kecamatan', select_kabupaten.children("option:selected").val(), select_kecamatan.children("option:selected").val());
    getRegion('kelurahan', select_kecamatan.children("option:selected").val(), select_kelurahan.children("option:selected").val());

    table = $('#datatable').DataTable({
      dom: '<"html5buttons">iBflrtp',
      // responsive: true,
      processing: true,
      serverSide: true,
      // fixedHeader: true,
      "lengthMenu": [ [10, 25, 50, 100, 1000, -1], [10, 25, 50, 100, 1000, "All"] ],
      scrollX: true,
      ajax: {
          "url"  : "{{ route('table.user') }}", 
          "data" : function (d) {
            d.id_prov = select_provinsi.children("option:selected").val();
            d.id_kab = select_kabupaten.children("option:selected").val();
            d.id_kec = select_kecamatan.children("option:selected").val();
            d.id_kel = select_kelurahan.children("option:selected").val();
            d.gender = $('#select_gender').children("option:selected").val();
            d.blood = $('#select_blood').children("option:selected").val();
            d.education_id = $('#select_education').children("option:selected").val();
            d.job_id = $('#select_job').children("option:selected").val();
            d.salary_id = $('#select_salary').children("option:selected").val();
            d.married = $('#select_status').children("option:selected").val();
            d.has_die = $('#select_is_alive').children("option:selected").val();
            d.min_age = $('#min_age').children("option:selected").val();
            d.max_age = $('#max_age').children("option:selected").val();
            d.jamaah = $('#jamaah').children("option:selected").val();
          }
      },
      columns: [
        {data: 'DT_RowIndex', name: 'id'},
        {data: 'name', name: 'name'},
        {data: 'nik', name: 'nik'},
        {data: 'gender', name: 'gender', searchable: 'false'},
        {data: 'blood', name: 'blood', searchable: 'false'},
        {data: 'birth_date', name: 'birth_date'},
        {data: 'DT_RowData.age', name: 'birth_date', searchable: 'false'},
        {data: 'education.education', name: 'education_id', searchable: 'false'},
        {data: 'job.job', name: 'job_id', searchable: 'false'},
        {data: 'salary.name', name: 'salary_id', searchable: 'false'},
        {data: 'married', name: 'married', searchable: 'false'},
        {data: 'DT_RowData.jamaah', name: 'jamaah', searchable: 'false', orderable: 'false'},
        {data: 'provinsi.nama', name: 'prov_id', searchable: 'false'},
        {data: 'kabupaten.nama', name: 'kab_id', searchable: 'false'},
        {data: 'kecamatan.nama', name: 'kec_id', searchable: 'false'},
        {data: 'kelurahan.nama', name: 'kel_id', searchable: 'false'},
        {data: 'DT_RowData.has_die', name: 'has_die', searchable: 'false'},
        {data: 'action', name: 'action'},
      ]
    });

    $(document).on("change", "select[name='select_provinsi']" , function() {
        id_province = $(this).children("option:selected").val();
        select_kabupaten.html('<option value="">pilih Kabupaten</option>')
        select_kecamatan.html('<option value="">pilih Kecamatan</option>')
        select_kelurahan.html('<option value="">pilih Kelurahan</option>')
        getRegion('kabupaten', id_province);
        table.draw();
    });

    $(document).on("change", "select[name='select_kabupaten']" , function() {
        id_kabupaten = $(this).children("option:selected").val();
        select_kecamatan.html('<option value="">pilih Kecamatan</option>')
        select_kelurahan.html('<option value="">pilih Kelurahan</option>')
        getRegion('kecamatan', id_kabupaten);
        table.draw();
    });

    $(document).on("change", "select[name='select_kecamatan']" , function() {
        id_kecamatan = $(this).children("option:selected").val();
        select_kelurahan.html('<option value="">pilih Kelurahan</option>')
        getRegion('kelurahan', id_kecamatan);
        table.draw();
    });

    $(document).on("change", "select[name='select_kelurahan']" , function() {
        table.draw();
    });

    arr_select.forEach(function (key, value) {
        $('#' + key).on('change', function(e) {
          cekAge = checkAge()
          if(cekAge == 0) {return}
          console.log('run tes')
          table.draw();
        })
    });

    function checkAge() {
      console.log('tes cek age')
      var min_age = parseInt($('#min_age').val())
      var max_age = parseInt($('#max_age').val())

      if(max_age < min_age && min_age != null && max_age != null) {
        alert('max age tidak boleh lebih kecil')
        return 0;
      }
    }


    // $('#select_gender').on('change', function(e) {
    //   table.draw();
    // })

    function getRegion($get_by, $region_id, $is_selected = '') {
      $.ajax({
        url: "{{route('get.region')}}",
        method: "POST",
        "data": {
          '_token' : csrf_token,
          'get_by' : $get_by,
          'region_id' : $region_id,
          'is_selected' : $is_selected,
        },
        success: function(res) {
          if($get_by == 'provinsi') {
            select_provinsi.html(res)
          }
          if($get_by == 'kabupaten') {
            select_kabupaten.html(res)
          }
          if($get_by == 'kecamatan') {
            select_kecamatan.html(res)
          }
          if($get_by == 'kelurahan') {
            select_kelurahan.html(res)
          }
        }
      })
    }
  })
  
</script>
@endsection