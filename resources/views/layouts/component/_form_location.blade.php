@php
// dd($arr_prov)
@endphp

@if($role == 'Admin Provinsi' || $role == 'Admin Kabupaten' || $role == 'Admin Kecamatan' || $role == 'Admin Kelurahan')
	<input type="hidden" name="prov_id" value="{{$provinsi->id}}">
	{!! Form::dropdownRaw('prov_id' , 'Provinsi', [$provinsi->id => $provinsi->nama], $provinsi->id, ['disabled']) !!}
@else
	{!! Form::dropdownRaw('prov_id' , 'Provinsi', $arr_prov, $prov_id) !!}
@endif

@if($role == 'Admin Kabupaten' || $role == 'Admin Kecamatan' || $role == 'Admin Kelurahan')
	<input type="hidden" name="kab_id" value="{{$kabupaten->id}}">
	{!! Form::dropdownRaw('kab_id' , 'Kabupaten/Kota', [$kabupaten->id => $kabupaten->nama], null, ['disabled']) !!}
@else
    {!! Form::dropdownRaw('kab_id' , 'Kabupaten/Kota', ['' => 'pilih Kabupaten/Kota']) !!}
@endif

@if($role == 'Admin Kecamatan' || $role == 'Admin Kelurahan')
	<input type="hidden" name="kec_id" value="{{$kecamatan->id}}">
	{!! Form::dropdownRaw('kec_id' , 'Kecamatan', [$kecamatan->id => $kecamatan->nama], null, ['disabled']) !!}
@else
    {!! Form::dropdownRaw('kec_id' , 'Kecamatan', ['' => 'pilih Kecamatan']) !!}
@endif

@if($role == 'Admin Kelurahan')
	<input type="hidden" name="kel_id" value="{{$kelurahan->id}}">
    {!! Form::dropdownRaw('kel_id' , 'Kelurahan', [$kelurahan->id => $kelurahan->nama], null, ['disabled']) !!}
@else
    {!! Form::dropdownRaw('kel_id' , 'Kelurahan', ['' => 'pilih Kelurahan']) !!}
@endif

