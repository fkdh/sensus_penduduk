@php
// dd($arr_prov)
@endphp

@if($role == 'Admin Provinsi' || $role == 'Admin Kabupaten' || $role == 'Admin Kecamatan' || $role == 'Admin Kelurahan')
<div class="col-md-3">
    {!! Form::dropdownRaw('select_provinsi' , 'Provinsi' , [$provinsi->id => $provinsi->nama], null, ['disabled']) !!}
</div>
@else
<div class="col-md-3">
    {!! Form::dropdownRaw('select_provinsi' , 'Provinsi' , $arr_prov) !!}
</div>
@endif

@if($role == 'Admin Kabupaten' || $role == 'Admin Kecamatan' || $role == 'Admin Kelurahan')
<div class="col-md-3">
    {!! Form::dropdownRaw('select_kabupaten' , 'Kabupaten' , [$kabupaten->id => $kabupaten->nama], null, ['disabled']) !!}
</div>
@else
<div class="col-md-3">
    {!! Form::dropdownRaw('select_kabupaten' , 'Kabupaten' , ['' => 'pilih Kabupaten']) !!}
</div>
@endif

@if($role == 'Admin Kecamatan' || $role == 'Admin Kelurahan')
<div class="col-md-3">
    {!! Form::dropdownRaw('select_kecamatan' , 'Kecamatan' , [$kecamatan->id => $kecamatan->nama], null, ['disabled']) !!}
</div>
@else
<div class="col-md-3">
    {!! Form::dropdownRaw('select_kecamatan' , 'Kecamatan' , ['' => 'pilih Kecamatan']) !!}
</div>
@endif

@if($role == 'Admin Kelurahan')
<div class="col-md-3">
    {!! Form::dropdownRaw('select_kelurahan' , 'Kelurahan' , [$kelurahan->id => $kelurahan->nama], null, ['disabled']) !!}
</div>
@else
<div class="col-md-3">
    {!! Form::dropdownRaw('select_kelurahan' , 'Kelurahan' , ['' => 'pilih Kelurahan']) !!}
</div>
@endif

