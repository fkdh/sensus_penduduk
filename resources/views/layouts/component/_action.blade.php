@if(!empty($url_show))
<a href="{{ $url_show }}" class="btn-show" target="_blank" title="Detail: {{ $title }}"><i class="fas fa-eye text-primary" ></i></a> | 
@endif

@if(!empty($url_edit))
<a href="{{ $url_edit }}" class="modal-show edit" title="Edit {{ $title }}"><i class="fas fa-edit text-warning"></i></a> | 
@endif

@if(!empty($url_destroy))
<a href="{{ $url_destroy }}" class="btn-delete" title="{{ $title }}"><i class="fas fa-trash text-danger"></i></a> | 
@endif

@if(!empty($url_set_alive))
<a href="{{ $url_set_alive }}" class="modal-show edit" title="Set Hidup {{ $title }}">
	<i class="fas fa-medkit text-success"></i>
</a> |
@endif

@if(!empty($url_set_die))
<a href="{{ $url_set_die }}" class="modal-show edit" title="Set Tanggal Meninggal {{ $title }}">
	<i class="fas fa-skull-crossbones text-danger"></i>
</a>
@endif