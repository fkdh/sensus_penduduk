  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src={{asset('adminLTE/plugins/jquery/jquery.min.js')}}></script>
<!-- Bootstrap 4 -->
<script src={{asset('adminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js')}}></script>
<!-- DataTables -->
<script src="{{asset('adminLTE/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('adminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('adminLTE/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('adminLTE/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<!-- Datatable Button -->
<script src="{{asset('adminLTE/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('adminLTE/plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('adminLTE/plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('adminLTE/plugins/datatables-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('adminLTE/plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('adminLTE/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>

{{-- Fixed Heder --}}
<script src="{{asset('adminLTE/plugins/datatables-fixedheader/js/dataTables.fixedheader.min.js')}}"></script>
<script src="{{asset('adminLTE/plugins/datatables-fixedheader/js/fixedHeader.bootstrap4.min.js')}}"></script>


<script src="{{asset('adminLTE/plugins/jszip/jszip.min.js')}}"></script>
{{-- <script src="{{asset('adminLTE/plugins/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('adminLTE/plugins/pdfmake/vfs_fonts.js')}}"></script> --}}
<!-- Croopie -->
<script src="{{asset('adminLTE/plugins/croppie/croppie.min.js')}}"></script>
<!-- SweetAlert2 -->
<script src="{{asset('adminLTE/plugins/sweetalert2/sweetalert2.all.min.js')}}"></script>
<!-- Select2 -->
<script src="{{asset('adminLTE/plugins/select2/js/select2.full.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('adminLTE/plugins/daterangepicker/moment.min.js')}}"></script>
<script src="{{asset('adminLTE/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- AdminLTE App -->
<script src={{asset('adminLTE/dist/js/adminlte.min.js')}}></script>
<!-- AdminLTE for demo purposes -->
<script src={{asset('adminLTE/dist/js/demo.js')}}></script>

{{-- app JS --}}
<script src={{asset('js/app.js')}}></script>

{{-- Custom Script --}}
@yield('script')

