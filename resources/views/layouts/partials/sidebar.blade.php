@php
$menu = config('menu.main_menu');
$user = Auth::user();
$role = config('global.role');
$role = $role[$user->role];
// dd($role);

@endphp
<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src='{{asset('adminLTE/dist/img/AdminLTELogo.png')}}' alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AdminPengamal</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src= '{{asset('adminLTE/dist/img/user2-160x160.jpg')}}' class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{$user->name}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          @foreach($menu as $menu_row)
          @if(empty($menu_row['sub_menu']) && (isset($menu_row['sub_menu'])))
            <li class="nav-item">
              <a href="{{url($menu_row['main_url'])}}" class="nav-link">
                <i class="{{$menu_row['class']}}"></i>
                <p>{{$menu_row['desc']}}</p>
              </a>
            </li>
          @else
          <li class="nav-item has-treeview"
            @if($menu_row['desc'] == 'Master Data')
              @if($role == 'Super Admin')
                style="display: show"
              @else
                style="display: none"
              @endif
            @endif
          >
            <a href="#" class="nav-link">
              <i class="{{$menu_row['class']}}"></i>
              <p>
                {{$menu_row['desc']}}
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              @foreach($menu_row['sub_menu'] as $submenu_row)

              <li class="nav-item">
                <a href="{{url($submenu_row['main_url'])}}" class="nav-link">
                  <i class="{{$submenu_row['class']}}"></i>
                  <p>{{$submenu_row['desc']}}</p>
                </a>
              </li>
              @endforeach
            </ul>
          </li>

          @endif
          @endforeach
          <li class="nav-item">
            <a href="{{route('logout')}}" class="nav-link" id="btn-logout">
              <i class="nav-icon far fa fa-sign-out-alt"></i>
              <p>Logout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
