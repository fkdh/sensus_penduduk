<!DOCTYPE html>
<html>
@include('layouts.partials.header')
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
<!-- Navbar -->
@include('layouts.partials.navbar')
<!-- /.navbar -->

<!-- Main Sidebar Container -->
@include('layouts.partials.sidebar')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
@include('layouts.partials/header_wrap')

<!-- Main content -->
<section class="content">

@yield('content')

{{-- include modal --}}
@include('layouts.component._modal')

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@include('layouts.partials.footer')
</body>
</html>
