{!! Form::model($model, [
    'route' => $model->exists ? ['kk.update', $model->id] : 'kk.store',
    'method' => $model->exists ? 'PUT' : 'POST'
]) !!}

<input type="hidden" name="upload_kk" id="upload_kk" value="">

@if(!$model->exists)
{!! Form::dropdownRaw('id_pengamal' , 'Nama Kepala Keluarga') !!}
@endif
{!! Form::textRaw('no_kk' , 'No. KK') !!}
{!! Form::fileRaw('kk_path' , 'Foto KK : ') !!}
<input type="hidden" name="kk_path_send" id="kk_path_send" value="{{$model->kk_path ? 'exist' : ''}}">
<p>maksimal foto 1 MB</p>
<div id="image-preview"></div>

{!! Form::close() !!}

<script>
	var module_url = "{{route('get.pengamal.list')}}"
    var image_crop = ''
    var reader = ''
    var img_path = "{{$model->kk_path}}"
    var img_model = "{{asset($model->kk_path)}}"

	function formatRepo (repo) {
        if (repo.loading) return repo.text;
        var markups = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'>Name: " + repo.name + "</div>"+
            "<div class='select2-result-repository__description'>NIK :" + repo.nik + "</div>"
            +"</div></div>";
        return markups;
    }

    function formatRepoSelection (repo) {
        return repo.name || repo.text;
    }
	$(document).ready(function() {
        console.log(img_path)
        reader = new FileReader();
        image_crop = $('#image-preview').croppie({
          enableExif:true,
          viewport:{
            width:323,
            height:220,
          },
          boundary:{
            width:'100%',
            height:300
          }
        });

        if(img_path !== '') {
          image_crop.croppie('bind', {
            url:img_model
          })
        }

        function cek_img(img) {
          var size_max = 1024 * 1000
          var size_img = img.files[0].size
          var ext = $(img).val().split('.').pop().toLowerCase();
          if ($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
            alert('Image Format Harus JPG, JPEG, PNG or GIF.')
            return 0
          } else {
            if(size_img > size_max) {
              alert('File tidak boleh lebih dari 1 Mb')
              return 0
            }
          }
          return 1
        }

        $('#kk_path').change(function(){
          if(cek_img(this) == 0) {
            $('#kk_path').val('');
            return
          } else {
            $('#kk_path_send').val('exist')
          }
          reader.onload = function(event){
            // console.log(event.target.result)
            image_crop.croppie('bind', {
              url:event.target.result
            }).then(function(){
              console.log('jQuery bind complete');
            });
          }
          reader.readAsDataURL(this.files[0]);
        });

        $('#modal-btn-save').on('click', function() {
          image_crop.croppie('result', {
            type:'canvas',
            size:'original'
          }).then(function(response){
            $('#upload_kk').val(response)
          });
        })

		$('#id_pengamal').select2({
            ajax: {
                url: module_url,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                    q: params.term, // search term
                    page: params.page,
                    };
                },
                processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                results: data.items,
                pagination: {
                    more: (params.page * 20) < data.total_count
                }
                };
                },
                cache : true,
	        },
	        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
	        minimumInputLength: 1,
	        templateResult: formatRepo, // omitted for brevity, see the source of this page
	        templateSelection: formatRepoSelection, // omitted for brevity, see the source of this page
	        placeholder: 'Ketikan Nama / NIK',
	        });
	})
</script>
