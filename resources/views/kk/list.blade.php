@extends('layouts.master')

@section('title') 
  Kk
@endsection

@section('head_title') 
  KK
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">KK</a></li>
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Filter</h3>
  
          <div class="card-tools">
            {{-- <a href="{{route('role.create')}}" class="btn btn-success btn-sm modal-show" title="Tambah Role"><i class="fas fa-plus"></i> Tambah</a> --}}
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button> --}}
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <div class="form-group row filter-location">
            {!! $filter_location !!}
          </div>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Data</h3>

        <div class="card-tools">
          <a href="{{route('kk.create')}}" class="btn btn-success btn-sm modal-show" title="Tambah Kk"><i class="fas fa-plus"></i> Tambah</a>
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button> --}}
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        {{-- Table Field --}}
        <table id="datatable" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Kepala Keluarga</th>
              <th>No. KK</th>
              <th>Provinsi</th>
              <th>Kabupaten / Kota</th>
              <th>Kecamatan</th>
              <th>Kelurahan</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
  </div>
</div>
</div>
@endsection('')


@section('script')
<script>
  var csrf_token = $('meta[name="csrf-token"]').attr('content');
  var table
  var arr_select_2 = ['select_provinsi','select_kabupaten','select_kecamatan','select_kelurahan',]

  var select_provinsi = $('#select_provinsi')
  var select_kabupaten = $('#select_kabupaten')
  var select_kecamatan = $('#select_kecamatan')
  var select_kelurahan = $('#select_kelurahan')

  $(document).ready(function() {
    arr_select_2.forEach(function (key, value) {
        $('#' + key).select2()
    });

    getRegion('kabupaten', select_provinsi.children("option:selected").val(), select_kabupaten.children("option:selected").val());
    getRegion('kecamatan', select_kabupaten.children("option:selected").val(), select_kecamatan.children("option:selected").val());
    getRegion('kelurahan', select_kecamatan.children("option:selected").val(), select_kelurahan.children("option:selected").val());

    table = $('#datatable').DataTable({
        dom: '<"html5buttons">iBflrtp',
        responsive: false,
        processing: true,
        serverSide: true,
        "lengthMenu": [ [10, 25, 50, 100, 1000, -1], [10, 25, 50, 100, 1000, "All"] ],
        scrollX: true,
        ajax: {
          url : "{{ route('table.kk') }}",
          data : function(d) {
            d.id_prov = select_provinsi.children("option:selected").val();
            d.id_kab = select_kabupaten.children("option:selected").val();
            d.id_kec = select_kecamatan.children("option:selected").val();
            d.id_kel = select_kelurahan.children("option:selected").val();
          }
        },
        columns: [
          {data: 'DT_RowIndex', name: 'id'},
          {data: 'name', name: 'pengamal.name'},
          {data: 'no_kk', name: 'no_kk'},
          {data: 'DT_RowData.provinsi', name: 'prov_id', searchable: 'false'},
          {data: 'DT_RowData.kabupaten', name: 'kab_id', searchable: 'false'},
          {data: 'DT_RowData.kecamatan', name: 'kec_id', searchable: 'false'},
          {data: 'DT_RowData.kelurahan', name: 'kel_id', searchable: 'false'},
          {data: 'action', name: 'action'}
        ]
    });

    $(document).on("change", "select[name='select_provinsi']" , function() {
        id_province = $(this).children("option:selected").val();
        select_kabupaten.html('<option value="">pilih Kabupaten</option>')
        select_kecamatan.html('<option value="">pilih Kecamatan</option>')
        select_kelurahan.html('<option value="">pilih Kelurahan</option>')
        getRegion('kabupaten', id_province);
        table.draw();
    });

    $(document).on("change", "select[name='select_kabupaten']" , function() {
        id_kabupaten = $(this).children("option:selected").val();
        select_kecamatan.html('<option value="">pilih Kecamatan</option>')
        select_kelurahan.html('<option value="">pilih Kelurahan</option>')
        getRegion('kecamatan', id_kabupaten);
        table.draw();
    });

    $(document).on("change", "select[name='select_kecamatan']" , function() {
        id_kecamatan = $(this).children("option:selected").val();
        select_kelurahan.html('<option value="">pilih Kelurahan</option>')
        getRegion('kelurahan', id_kecamatan);
        table.draw();
    });

    $(document).on("change", "select[name='select_kelurahan']" , function() {
        table.draw();
    });

    function getRegion($get_by, $region_id, $is_selected = '') {
      $.ajax({
        url: "{{route('get.region')}}",
        method: "POST",
        "data": {
          '_token' : csrf_token,
          'get_by' : $get_by,
          'region_id' : $region_id,
          'is_selected' : $is_selected,
        },
        success: function(res) {
          if($get_by == 'provinsi') {
            select_provinsi.html(res)
          }
          if($get_by == 'kabupaten') {
            select_kabupaten.html(res)
          }
          if($get_by == 'kecamatan') {
            select_kecamatan.html(res)
          }
          if($get_by == 'kelurahan') {
            select_kelurahan.html(res)
          }
        }
      })
    }
  })
  
</script>
@endsection