<div class="form-group" id="{{ $name }}-cont">
    {{ Form::label($name, $label, ['class' => 'control-label']) }}
    {{ Form::file($name) }}
</div>