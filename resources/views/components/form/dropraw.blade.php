<div class="form-group">
    {{ Form::label($name, $label, ['class' => 'control-label']) }}
    {!! Form::select($name, $list_dropdown, $value, array_merge(['class' => 'form-control'] , $attributes)) !!}
</div>