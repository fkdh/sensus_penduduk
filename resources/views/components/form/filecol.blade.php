<div class="form-group" id="{{ $name }}-cont">
    {{ Form::label($name, $label, ['class' => 'control-label col-md-3']) }}
    <div class="col-md-9">
        {{ Form::file($name) }}
    </div>
</div>