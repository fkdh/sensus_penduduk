@if (session('message'))
    <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {{ session('message') }}
    </div>
@endif
@if (session('message_failed'))
    <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {{ session('message_failed') }}
    </div>
@endif