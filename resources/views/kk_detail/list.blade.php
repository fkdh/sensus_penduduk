@extends('layouts.master')

@section('title') 
  Kk Detail
@endsection

@section('head_title') 
  KK Detail
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">KK</a></li>
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">No.KK : {{$model->no_kk}}</h3>

        <div class="card-tools">
          <a href="{{route('kk-detail.create', $model->id)}}" class="btn btn-success btn-sm modal-show" title="Tambah Kk"><i class="fas fa-plus"></i> Tambah</a>
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button> --}}
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        {{-- Table Field --}}
        <table id="datatable" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Name</th>
              <th>NIK</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
  </div>
</div>
</div>
@endsection('')


@section('script')
<script>
  var id_kk = "{{$model->id}}"
  var table
  table = $('#datatable').DataTable({
      dom: '<"html5buttons">iBflrtp',
      responsive: false,
      processing: true,
      serverSide: true,
      "lengthMenu": [ [-1], ["All"] ],
      scrollX: false,
      ajax: {
        url: "{{ route('table.kk.detail') }}",
        data: function(d) {
          d.id_kk = id_kk;
        }
      },
      columns: [
          {data: 'DT_RowIndex', name: 'id'},
          {data: 'name', name: 'name'},
          {data: 'nik', name: 'nik'},
          {data: 'user_as', name: 'user_as'},
          {data: 'action', name: 'action'}
      ]
  });
</script>
@endsection