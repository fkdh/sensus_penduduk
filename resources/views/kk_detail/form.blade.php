{!! Form::model($model, [
    'route' => $model->exists ? ['kk-detail.update', $model->id] : 'kk-detail.store',
    'method' => $model->exists ? 'PUT' : 'POST'
]) !!}

@if(!empty($id_kk))
<input type="hidden" name="id_kk" value="{{$id_kk}}">
@endif

@if(!$model->exists)
{!! Form::dropdownRaw('id_pengamal' , 'Nama') !!}
@endif

{!! Form::dropdownRaw('user_as' , 'Status Keluarga', config('select.status_keluarga')) !!}

{!! Form::close() !!}

<script>
	var module_url = "{{route('get.pengamal.list')}}"
	console.log(module_url)
	function formatRepo (repo) {
        if (repo.loading) return repo.text;
        var markups = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'>Name: " + repo.name + "</div>"+
            "<div class='select2-result-repository__description'>NIK :" + repo.nik + "</div>"
            +"</div></div>";
        return markups;
    }

    function formatRepoSelection (repo) {
        return repo.name || repo.text;
    }
	$(document).ready(function() {
		$('#id_pengamal').select2({
            ajax: {
                url: module_url,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                    q: params.term, // search term
                    page: params.page,
                    };
                },
                processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                results: data.items,
                pagination: {
                    more: (params.page * 20) < data.total_count
                }
                };
                },
                cache : true,
	        },
	        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
	        minimumInputLength: 1,
	        templateResult: formatRepo, // omitted for brevity, see the source of this page
	        templateSelection: formatRepoSelection, // omitted for brevity, see the source of this page
	        placeholder: 'Ketikan Nama / NIK',
	        });
	})
</script>
