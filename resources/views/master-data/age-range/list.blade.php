@extends('layouts.master')

@section('title') 
  Age Range
@endsection

@section('head_title') 
  Age Range
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">Age Range</a></li>
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Data</h3>

        <div class="card-tools">
          <a href="{{route('age-range.create')}}" class="btn btn-success btn-sm modal-show" title="Tambah"><i class="fas fa-plus"></i> Tambah</a>
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button> --}}
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        {{-- Table Field --}}
        <table id="datatable" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Name</th>
              <th>Min Age</th>
              <th>Max Age</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
  </div>
</div>
</div>
@endsection('')


@section('script')
  <script>
        $('#datatable').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: "{{ route('table.age.range') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'min_age', name: 'min_age'},
                {data: 'max_age', name: 'max_age'},
                {data: 'action', name: 'action'}
            ]
        });
    </script>
@endsection