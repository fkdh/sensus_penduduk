{!! Form::model($model, [
    'route' => $model->exists ? ['age-range.update', $model->id] : 'age-range.store',
    'method' => $model->exists ? 'PUT' : 'POST'
]) !!}

{!! Form::textRaw('name' , 'Nama') !!}
{!! Form::textRaw('min_age' , 'Min Age') !!}
{!! Form::textRaw('max_age' , 'Max Age') !!}

{!! Form::close() !!}
