{!! Form::model($model, [
    'route' => $model->exists ? ['admin.update', $model->id] : 'admin.store',
    'method' => $model->exists ? 'PUT' : 'POST'
]) !!}

<input type="hidden" name="url-get-region" value="{{route('get.region')}}">

{!! Form::textRaw('name' , 'Name') !!}
{!! Form::textRaw('email' , 'Email') !!}
{!! Form::textRaw('phone' , 'Phone') !!}
{!! Form::passRaw('password' , 'Password') !!}
{!! Form::passRaw('password_confirmation' , 'Password Confirmation') !!}
{!! Form::dropdownRaw('role' , 'Role' , $role) !!}
{!! Form::dropdownRaw('prov_id' , 'Provinsi', $arr_prov) !!}
{!! Form::dropdownRaw('kab_id' , 'Kabupaten/Kota', ['' => 'pilih Kabupaten/Kota']) !!}
{!! Form::dropdownRaw('kec_id' , 'Kecamatan', ['' => 'pilih Kecamatan']) !!}
{!! Form::dropdownRaw('kel_id' , 'Kelurahan', ['' => 'pilih Kelurahan']) !!}
{!! Form::dropdownRaw('is_active' , 'is Active' , config('select.is_active')) !!}

{!! Form::close() !!}

<script>
  var url_getRegion = $('input[name="url-get-region"]').val()
      id_province = '{{$model->prov_id}}'
      id_kabupaten = '{{$model->kab_id}}'
      id_kecamatan = '{{$model->kec_id}}'

      selected_kab_id = '{{$model->kab_id}}'
      select_kabupaten = $('#kab_id')
      selected_kec_id = '{{$model->kec_id}}'
      select_kecamatan = $('#kec_id')
      selected_kel_id = '{{$model->kel_id}}'
      select_kelurahan = $('#kel_id')

      arr_select_2 = ['prov_id', 'kab_id', 'kec_id', 'kel_id'];

  $(document).ready(function() {
    getRegion(url_getRegion,select_kabupaten, 'kabupaten', id_province, selected_kab_id);
    getRegion(url_getRegion,select_kecamatan, 'kecamatan', id_kabupaten, selected_kec_id);
    getRegion(url_getRegion,select_kelurahan, 'kelurahan', id_kecamatan, selected_kel_id);

    arr_select_2.forEach(function (key, value) {
        $('#' + key).select2()
    });        

    $(document).on("change", "#prov_id" , function() {
        id_province = $(this).children("option:selected").val();

        // blank
        $('#kab_id').html('<option value="">pilih Kabupaten</option>')
        $('#kec_id').html('<option value="">pilih Kecamatan</option>')
        $('#kel_id').html('<option value="">pilih Kelurahan</option>')

        select_kabupaten = $('#kab_id')
        getRegion(url_getRegion,select_kabupaten, 'kabupaten', id_province, selected_kab_id);
    });

    $(document).on("change", "#kab_id" , function() {
        url_getRegion = $('input[name="url-get-region"]').val()
        id_kabupaten = $(this).children("option:selected").val();

        // blank
        $('#kec_id').html('<option value="">pilih Kecamatan</option>')
        $('#kel_id').html('<option value="">pilih Kelurahan</option>')

        select_kecamatan = $('#kec_id')
        getRegion(url_getRegion,select_kecamatan, 'kecamatan', id_kabupaten);
    });

    $(document).on("change", "#kec_id" , function() {
        url_getRegion = $('input[name="url-get-region"]').val()
        id_kecamatan = $(this).children("option:selected").val();
        select_kelurahan = $('#kel_id')
        getRegion(url_getRegion,select_kelurahan, 'kelurahan', id_kecamatan);
    });
  

  	function getRegion(url, select_region, get_by, region_id, is_selected = '') {
      $.ajax({
        url: url,
        method: "POST",
        "data": {
          '_token' : csrf_token,
          'get_by' : get_by,
          'region_id' : region_id,
          'is_selected' : is_selected,
        },
        success: function(res) {
          if(get_by == 'provinsi') {
            select_region.html(res)
          }
          if(get_by == 'kabupaten') {
            select_region.html(res)
          }
          if(get_by == 'kecamatan') {
            select_region.html(res)
          }
          if(get_by == 'kelurahan') {
            select_region.html(res)
          }
        }
      })
    }
  })

  
</script>
