@extends('layouts.master')

@section('title') 
  Admin
@endsection

@section('head_title') 
  Admin
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">Admin</a></li>
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">DataTable with minimal features & hover style</h3>

        <div class="card-tools">
          <a href="{{route('admin.create')}}" class="btn btn-success btn-sm modal-show" title="Tambah Admin"><i class="fas fa-plus"></i> Tambah</a>
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button> --}}
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        {{-- Table Field --}}
        <table id="datatable" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Name</th>
              <th>Phone</th>
              <th>Email</th>
              <th>Role</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
  </div>
</div>
</div>
@endsection('')


@section('script')
<script>
  var csrf_token = $('meta[name="csrf-token"]').attr('content');
  $('#datatable').DataTable({
      responsive: true,
      processing: true,
      serverSide: true,
      ajax: "{{ route('table.admin') }}",
      columns: [
          {data: 'DT_RowIndex', name: 'id'},
          {data: 'name', name: 'name'},
          {data: 'phone', name: 'phone'},
          {data: 'email', name: 'email'},
          {data: 'DT_RowData.role', name: 'role'},
          {data: 'DT_RowData.is_active', name: 'is_active'},
          {data: 'action', name: 'action'}
      ]
  });
</script>
@endsection