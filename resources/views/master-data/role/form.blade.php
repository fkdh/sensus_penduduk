{!! Form::model($model, [
    'route' => $model->exists ? ['role.update', $model->id] : 'role.store',
    'method' => $model->exists ? 'PUT' : 'POST'
]) !!}

{!! Form::textRaw('role' , 'Role') !!}

{!! Form::close() !!}
