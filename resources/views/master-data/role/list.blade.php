@extends('layouts.master')

@section('title') 
  Role
@endsection

@section('head_title') 
  Role
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="#">Role</a></li>
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Data</h3>

        <div class="card-tools">
          <a href="{{route('role.create')}}" class="btn btn-success btn-sm modal-show" title="Tambah Role"><i class="fas fa-plus"></i> Tambah</a>
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button> --}}
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        {{-- Table Field --}}
        <table id="datatable" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Role</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
  </div>
</div>
</div>
@endsection('')


@section('script')
  <script>
        $('#datatable').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: "{{ route('table.role') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'id'},
                {data: 'role', name: 'role'},
                {data: 'action', name: 'action'}
            ]
        });
    </script>
@endsection